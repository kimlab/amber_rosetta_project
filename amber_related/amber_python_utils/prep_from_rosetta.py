#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 11:28:33 2017

@author: owenwhitley
"""
import parseText as pt
import os
import subprocess
import re

def aa_rename_rosetta_to_amber(pdb_filepath,output_folder,aa_mapping,overwrite = False):

    
#Purpose of function: rename NCAAs in pdbs with rosettaa named NCAAs     

#expected inputs: pdb filepath, with a pdb file that was outputted from rosetta
#               aa mapping as a filepath, with a csv file that contains the 
#               mapping orf rosetta names ot amber names

#output: pdb file with changed names

    
    pdbFileObj = open(pdb_filepath,'r')
    aa_map_fileObj = open(aa_mapping,'r')
    
    pdb_filename = pdb_filepath.split('/')[-1]
    output_filename = pdb_filename[0:-4] + '_ncaa_renam.pdb'
    output_filepath = os.path.join(output_folder,output_filename)
    
    
    #Are we overwriting? If not, and file with same name as output file exists,
    #don't make output file. If yes, remove the existing file with same name
    if (os.path.exists(output_filepath)) & (overwrite == False):
        raise ValueError('file exists with same name' + output_filepath + 
                         ' as output file, overwirte specified as False')
        
    elif os.path.exists(output_filepath):
        os.remove(output_filepath)
    
    output_file_obj = open(output_filename, 'a')
    
    aa_map_array = pt.parseText_RE(aa_map_fileObj,'[,\n]')
    
    rosetta_names = []
    amber_names = []
    seen_names = []
    
    #create list of rosetta NCAA names and corresponding list of amber NCAA names
    for i in range(1,len(aa_map_array),1):
        
        rosetta_names.append(aa_map_array[i][0])
        amber_names.append(aa_map_array[i][2])
        
    for line in pdbFileObj:
        
        #Iteratively go through pdb file line by line, editing
        #any lines with NCAA residue on ATOM or TER line
        
        
        
        if (line[0:4] == 'ATOM') | (line[0:3] == 'TER'):
            
            #If ATOM or TER line, see if residue name is in list of rosetta_names
            #If yes, find the index of the rosetta name, and find approrpiate
            #amber name. Reconstruct the line as a newLine, with new residue incorporated

            res_name = line[17:20]
            
            if res_name in rosetta_names:
                
                for n in range(0,len(rosetta_names)):
                    
                    if res_name == rosetta_names[n]:
                        #if you find a rosetta name matching the residue name, save index
                        amber_index = n
                        
                        break
                new_res_name = amber_names[amber_index]
                newLine = line[0:17] + new_res_name + line[20:len(line)]
                
                if (new_res_name in seen_names) == False:
                    
                    seen_names.append(new_res_name)
            else:
                newLine = line
                
        else:
            #If not an ATOM or TER line, just write the line to the file
            newLine = line
            
        output_file_obj.write(newLine)
    
    #Close all files that were opened in this function
    pdbFileObj.close()
    aa_map_fileObj.close()    
    output_file_obj.close()
    
    outputs = [output_filename,seen_names]
    return(outputs)
#==============================================================================
        
def strip_h2(in_pdb_file,out_pdb_file, overwrite = False):
    #same as strip_H, renamed so that if any differnces do arise, we have a 
    #reminder that the two functions exist separately in different modules
    #Function takes pdb and creates a copy with all Atom lines with a hydrogen
    #removed
    #Inputs: pdb file (filepath)
    #output: pdb file with every atom line containing 'H' in last column omitted (i.e. atom is hydrogen)
    #
    
    if (os.path.exists(out_pdb_file)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(out_pdb_file):
        os.remove(out_pdb_file)
    
    inp_file_obj = open(in_pdb_file, 'r')        
    output_file_obj = open(out_pdb_file, 'a')
    
    atom_count = 0
    for line in inp_file_obj:
        
        if line[0:4] == 'ATOM':
            
            if ('H' in line[77:78]) == False:
                atom_count = atom_count+1
                ATOM_section = line[0:6] #record name for line in PDB
                index_str = '     '
                index_str = index_str[0:(len(index_str) - len(str(atom_count)))] + str(atom_count)
                rest_of_line = line[11:len(line)]
                
                newLine = ATOM_section + index_str + rest_of_line
                output_file_obj.write(newLine)
                
        elif line[0:3] == 'TER':
            output_file_obj.write(line)
            
    inp_file_obj.close()
    output_file_obj.close()
            
#==============================================================================
    
def call_pdb4amber(input_pdb, #filepath to inputpdb
                   output_pdb,# filepath to pdb to be made
                   overwrite = False):
    
    #runs pdb4amber on input pdb, generates output pdb with atom names changed to amber style
    
    if (os.path.exists(output_pdb)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(output_pdb):
        os.remove(output_pdb)
    
    command_string = 'pdb4amber -i ' + input_pdb + ' -o ' + output_pdb
    subprocess.run(command_string.split(' ')) #runs pdb4amber -i input_pdb -o output_pdb

#==============================================================================
    
def call_reduce(input_pdb, #filepath to input pdb
                output_pdb, #filepath to output pdb to be made 
                overwrite = False):
    #runs ambertools reduce program to give hydrogens with amber style naming
    
    if (os.path.exists(output_pdb)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(output_pdb):
        os.remove(output_pdb)
                
    command_string = 'reduce ' + input_pdb
    output_pdb_fileobj = open(output_pdb,'a')
    subprocess.run(command_string.split(), stdout = output_pdb_fileobj)
    
    output_pdb_fileobj.close()

#==============================================================================
    
def auto_tleap(input_pdb, #filepath to pdb
               ffncaa_dir, #path to directory containing amber prep and frcmod files
               frcmod_list, #List of frcmod files,
               amber_prep, #Name of amber prep file. For now, we're sticking with the
                           #princeton group's ffncaa.in file from selene.princeton.edu
               expt_name, #generally, give pdb name_chain_residue_modification as a string
               output_dir, #Below this point, input variables/options not requiredto be specified
               overwrite = False,
               solvate = True,
               solvatebox_length = 11, #length (in angstroms) of the periodic boundaries for TIP3BOX
               addions = None, #generally, we will be adding ions to neutralize the system.
                               # specify 'Na+' for sodium and 'Cl-' for chloride
                               #Note: check leap log to make sure ions were added properly
                               #to inpcrd file before proceeding to relaxation and MD.
                               #Do not write a random string such as 'cats'
               forcefield = 'leaprc.ff14SB', #amber forcefield file
               ):
    
    #Inputs: a pdb, frcmod, and amber prep file (in our case,
    # a prepped pdb, and the proper ncaa frcmod and amber prep files from
    # ForceField_NCAA, http://selene.princeton.edu/FFNCAA/),
    # forcefield file for leap, water_model for leap.
    # ff14SB and tip3p are defaults for forcefield and water model, respectively
    #Outputs: a leap input file and coordinate (.crd) and topology (.prmtop)
    #files, ready for amber
    
    #make a file object at specified filepath
    
    tleap_filename = expt_name + '_tleap.in'
    tleap_filepath = os.path.join(output_dir,tleap_filename)
    
    if (os.path.exists(tleap_filepath)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(tleap_filepath):
        os.remove(tleap_filepath)
        
    tleap_fileobj = open(tleap_filepath, 'a')
    
    newString = ''
    ff_string = 'source ' + forcefield + ' #load forcefield\n'
    newString = newString + ff_string
    
    #If we are solvating system, we use the TIP3PBOX explicit solvent model
    if solvate == True:
        
        if (type(solvatebox_length) != float) & (type(solvatebox_length) != int):
            
            raise ValueError('if solvate is true, must specify solvatebox_length as float or integer value')
            
        
        solvation_box_str = 'source leaprc.tip3p #load water_model\n'
        newString = newString + solvation_box_str
        solvate_comm_str = 'solvatebox mol TIP3PBOX ' + str(solvatebox_length) + ' #creates explicit solvent\n'

    #If we provide an list with entries as frcmod files to load, write in that we need to load
    #the selene.princeton.edu prepin file (contains forcefield info) as well as the 
    #appropriate frcmod files
    if type(frcmod_list) == type([]):
        
        if len(frcmod_list) > 0:
            
            amber_prep_filepath = os.path.join(ffncaa_dir,amber_prep)    
            amber_prep_str = 'loadAmberPrep ' + amber_prep_filepath + ' #contains topology and force info where needed\n'
            
            for i in range(0,len(frcmod_list),1):
                
                frcmod_path = os.path.join(ffncaa_dir,frcmod_list[i])
                amber_param_str = 'loadAmberParams ' + frcmod_path + ' #loads additional force field modifiaction where needed \n'
                
    else:
         #otherwise, don't put anything in   
         amber_prep_str = ''
         amber_param_str = ''
    
    #write in the tleap file that we need to load the pdb, and to load it to the mol unit    
    load_pdb_str = 'mol = loadpdb ' + input_pdb + ' #loads the pdb into a unit variable in tleap\n'
    
    newString = newString + amber_prep_str + amber_param_str + load_pdb_str
    
    #If solvate is true, append the solvation command
    if solvate == True:
        
        newString = newString + solvate_comm_str
    #If addions has an input add it to the the commands   
    if addions != None :
    
        add_ion_str = 'addions mol ' + addions + ' 0 # add ions to neutralize system\n'
        newString = newString + add_ion_str
    
    #give names for the prmtop and inpcrd files    
    prmtop_name = expt_name + '.prmtop'
    inpcrd_name = expt_name + '.inpcrd'
   
    #put in a command to save the prmtop file and inpcrd file and then quit
    write_file_str = 'saveamberparm mol ' + prmtop_name + ' ' + inpcrd_name + ' #Save AMBER topology and inpcrd files \n'
    quit_line = 'quit #Quit tleap program\n'
   
    newString = newString + write_file_str + quit_line
    #actually write the file, then close it
    tleap_fileobj.write(newString)
    tleap_fileobj.close()
    return(tleap_filename, prmtop_name, inpcrd_name)

#==============================================================================

def prep_for_MD(    ambdir, #directory containing all relevant python modules for amber input prep and output processing
                            #this will be amber_rosetta_project/amber_related/
                    working_dir, #all inputs and outputs expected to be in same folder
                    ensemble_name,  #any name you want for the ensemble. serves as a prefix to all outputs
                    inpcrd, #coordinate file generated by tleap from pdb, filename
                    prmtop, #parameter/topology file, filename
                    rlx_solv_imin, #relax solvent (minimization) amber input file, filename
                    rlx_sys_imin, #relax system (minimization) amber input file, filename
                    heat_solv_mdin, #heat solvent (MD) amber input file, filename
                    heat_sys_mdin, #heat system (MD) amber input file, filename
                    prod_mdin, #production(MD) amber input file, filename
                    num_simulations, #number of simulations you wish to run
                    proc_per_sim, #processors per simulation. used to calculate number of processors needed
                                    #should be a multiple of 8,
                    mode = 'mpi-cpu', #two modes: mpi_cpu, which is meant for making inputs for 
                                        #pmemd.MPI, and cuda, which is meant for making inputs for pmemd.cuda
                    num_prod_steps = 1, #number of production steps you wish to run (you may have to run more than one
                                        #due to wall time limits on a cluster)
                    script_template = 'script_template.sh', #template for md bash scripts to be written
                    overwrite = True):
    
    #purpose: make a series of groupfiles and also record the sequence of files from relaxation to production
    
    default_dir = os.getcwd()
    os.chdir(working_dir)
    
    if overwrite == True:
        ovw = '-O'
    else:
        ovw = ''
        
    def overwrite_test(directory,filename,overwrite):
        
        filepath = os.path.join(directory,filename)
        if os.path.exists(filepath):
            
            if overwrite == True:
                
                os.remove(filepath)
                
            else:
                
                raise ValueError('overwrite != True and ' + filepath + ' exists')
    
    #make a relax script as a bash script
    rlx_script_filename = ensemble_name + '_rlx_protocol.sh'
    overwrite_test(working_dir,rlx_script_filename,overwrite)
    
    rlx_script_fileobj = open(rlx_script_filename, 'a')
    
    rlx_solv_output_name = ensemble_name + '_rlx_solv_imin.out'
    rlx_solv_start_crd = inpcrd
    rlx_solv_rst = ensemble_name + '_rlx_solv_imin.rst'
    
    rlx_sys_output_name = ensemble_name + '_rlx_sys_imin.out'
    rlx_sys_start_crd = rlx_solv_rst
    rlx_sys_rst = ensemble_name + '_rlx_sys_imin.rst'
    
    # write three lines to make a bash script. lines 2 and 3 contain the proper
    # pmemd commands to relax the solvent, then the system
    rlx_script_line1 = '#!/bin/bash \n'
    rlx_script_line2 = ('pmemd ' + ovw + ' -i ' + rlx_solv_imin + ' -o '
                      + rlx_solv_output_name + ' -c ' + rlx_solv_start_crd
                      + ' -p ' + prmtop + ' -ref ' + rlx_solv_start_crd
                      + ' -r ' + rlx_solv_rst + '\n')
    rlx_script_line3 = ('pmemd ' + ovw + ' -i ' + rlx_sys_imin + ' -o '
                      + rlx_sys_output_name + ' -c ' + rlx_sys_start_crd
                      + ' -p ' + prmtop + ' -ref ' + rlx_solv_start_crd 
                      + ' -r ' + rlx_sys_rst + '\n')
    
    rlx_solv_filestr = rlx_script_line1 + rlx_script_line2 + rlx_script_line3
    #makes entire string for bash file
    rlx_script_fileobj.write(rlx_solv_filestr)
    rlx_script_fileobj.close()
    
    comm_str1 = 'chmod +x ' + rlx_script_filename
    subprocess.run(comm_str1.split())
    
    if mode == 'mpi-cpu': #Under this mode, preparing inputs assuming an mpirun
                            #split across multiple cpus, with each node/cpu handling
                            #1 simulation. proc_per sim should be the max number
                            #of proc_per node
    
        #make the appropriate groupfile file objects
        heat_solv_groupfile_name = ensemble_name + '_heat_solv_groupfile.txt'
        overwrite_test(working_dir,heat_solv_groupfile_name,overwrite)
        heat_solv_groupfile = open(heat_solv_groupfile_name, 'a')
        
        heat_sys_groupfile_name = ensemble_name + '_heat_sys_groupfile.txt'
        overwrite_test(working_dir,heat_sys_groupfile_name,overwrite)
        heat_sys_groupfile = open(heat_sys_groupfile_name, 'a')
        
        
        for i in range(num_simulations):
            
            #for each i in the number of simulations, append appropriate lines to groupfiles
            #such that appropriate input and output filenames are written
            #the lines are constructed as strings formatted as you would expect for a groupfile
            #see amber 15 manual, multisander/multipmemd
            
            sim_num = str(i+1) #simulation number
            heat_solv_mdout_name = ensemble_name + '_heat_solv_md' + sim_num + '.mdout'
            heat_solv_mdcrd_name = ensemble_name + '_heat_solv_md' + sim_num + '.mdcrd'
            heat_solv_rst_name = ensemble_name + '_heat_solv_md' + sim_num + '.rst'
            heat_solv_line = (ovw + ' -i ' + heat_solv_mdin + ' -o ' + heat_solv_mdout_name +
                            ' -c ' + rlx_sys_rst + ' -p ' + prmtop + ' -x ' +
                            heat_solv_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                            ' -r ' + heat_solv_rst_name + '\n')
            
            heat_solv_groupfile.write(heat_solv_line)
            
            heat_sys_mdout_name = ensemble_name + '_heat_sys_md' + sim_num + '.mdout'
            heat_sys_mdcrd_name = ensemble_name + '_heat_sys_md' + sim_num + '.mdcrd'
            heat_sys_rst_name = ensemble_name + '_heat_sys_md' + sim_num + '.rst'
            heat_sys_line = (ovw + ' -i ' + heat_sys_mdin + ' -o ' + heat_sys_mdout_name +
                            ' -c ' + heat_solv_rst_name + ' -p ' + prmtop + ' -x ' +
                            heat_sys_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                            ' -r ' + heat_sys_rst_name + '\n')
            
            heat_sys_groupfile.write(heat_sys_line)
            
            mem_file_name = ensemble_name + '_mem_file_md' + sim_num + '.txt'
            overwrite_test(working_dir, mem_file_name, overwrite)
            mem_file_obj = open(mem_file_name, 'a')
            mem_file_str = (heat_solv_mdcrd_name + '\n' + heat_sys_mdcrd_name + '\n')
            mem_file_obj.write(mem_file_str)
            mem_file_obj.close()
            
        #Close file objects
        
        heat_solv_groupfile.close()
        heat_sys_groupfile.close()
        
        #make a bash script to run heat/equilibration protocols
        
        heat_md_name= ensemble_name + '_heat_md.sh'
        
        #see if file already exists, and overwrite them if overwrite == True
    
        overwrite_test(working_dir, heat_md_name, overwrite)
        
        #open the fileobjects for files to be created and template file
        
        heat_md_fileobj = open(heat_md_name, 'a')
        
        template_dir = os.path.join(ambdir,'data','scinet_script_templates')
        template_path = os.path.join(template_dir,script_template)
        template_fileobj= open(template_path, 'r')
        
        template_str = template_fileobj.read()
        heat_md_fileobj.write(template_str)
        
        
        num_processors = proc_per_sim*num_simulations
        #write the sequence of commands to be executed to the bash script
        
        mpirun_line1 = ('mpirun -np ' + str(num_processors) + ' pmemd.MPI -ng ' 
                        + str(num_simulations) + ' -groupfile ' 
                        + heat_solv_groupfile_name + ' \n')
        mpirun_line2 = ('mpirun -np ' + str(num_processors) + ' pmemd.MPI -ng ' 
                        + str(num_simulations) +' -groupfile ' 
                        + heat_sys_groupfile_name + ' \n')
        
        heat_md_fileobj.write(mpirun_line1)
        heat_md_fileobj.write(mpirun_line2)
        
        heat_md_fileobj.close() #close file
        
        comm_str2 = 'chmod +x ' + heat_md_name
        subprocess.run(comm_str2.split())
       
        #in below loop, make a groupfile for every production step. First
        #groupfile takes thte outputs of the heat sys protocol as the input coordinates,
        #rest take the last production step's restart file as input.
        #Also, add each new mdcrd filename to the appropriate mem_file
        
        for i in range(0,num_prod_steps,1):
            
            prod_step = str(i+1)
            prod_groupfile_name = ensemble_name + '_prod_step' + prod_step + '_groupfile.txt'
            overwrite_test(working_dir,prod_groupfile_name,overwrite)
            prod_groupfile = open(prod_groupfile_name, 'a')
            
            for n in range(0,num_simulations,1):
                
                sim_num = str(n + 1)
                #4 files: mdout, mdcrd (trajectory), restart file, restart file for last sim
                #used if i > 0, i.e. prod_step is 2 or higher
                prod_mdout_name = ensemble_name + '_prod_step' + prod_step + '_md' + sim_num + '.mdout'
                prod_mdcrd_name = ensemble_name + '_prod_step' + prod_step + '_md' + sim_num + '.mdcrd'
                prod_rst_name = ensemble_name + '_prod_step' + prod_step + '_md' + sim_num + '.rst'
                
                if i == 0:
                    heat_sys_rst_name = ensemble_name + '_heat_sys_md' + sim_num + '.rst'
                    prod_line = (ovw + ' -i ' + prod_mdin + ' -o ' + prod_mdout_name +
                                    ' -c ' + heat_sys_rst_name + ' -p ' + prmtop + ' -x ' +
                                    prod_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                                    ' -r ' + prod_rst_name + '\n')
                
                else: 
                    last_prod_rst_name = ensemble_name + '_prod_step' + str(i) + '_md' + sim_num + '.rst'
                    prod_line = (ovw + ' -i ' + prod_mdin + ' -o ' + prod_mdout_name +
                                    ' -c ' + last_prod_rst_name + ' -p ' + prmtop + ' -x ' +
                                    prod_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                                    ' -r ' + prod_rst_name + '\n')
                
                #assign the last_prod_rst_name as the current rst file name for the next iteration
                
                prod_groupfile.write(prod_line)
                
                #here we reopen mem-files to append appropriate mdcrd file names
                mem_file_name = ensemble_name + '_mem_file_md' + sim_num + '.txt'
                mem_file_obj = open(mem_file_name, 'a')
                mem_file_str = prod_mdcrd_name + '\n'
                mem_file_obj.write(mem_file_str)
                mem_file_obj.close()
                
            
                
            prod_groupfile.close()
            
            #for a given production step, make a bash script to run it
    
            prod_script_name = ensemble_name + '_prod_step' + prod_step + '.sh'
            prod_script_fileobj = open(prod_script_name,'a')
            prod_script_fileobj.write(template_str)
            mpirun_line3 = ('mpirun -np ' + str(num_processors) + ' pmemd.MPI -ng ' 
                        + str(num_simulations) +' -groupfile ' 
                        + prod_groupfile_name + ' \n') #This is the line that actually runs the pmemd program
            
            prod_script_fileobj.write(mpirun_line3)
            
            comm_str3 = 'chmod +x ' + prod_script_name
            subprocess.run(comm_str3.split())
            prod_script_fileobj.close()
            
    elif mode == 'cuda':    #Cuda mode is preparing inputs for one or more series of
                            #MD runs using a GPU
                            #If mode is cuda, we will be producing input files and scripts
                            # such that groupfiles are not used and pmemd.cuda is
                            #the command in the heating and production scripts.
        
        template_dir = os.path.join(ambdir,'data','scinet_script_templates')
        template_path = os.path.join(template_dir,script_template)
        template_fileobj= open(template_path, 'r')
        template_str = template_fileobj.read()
        
        for i in range(num_simulations):
            
            #for each simulation, make a script to equilibrate the system,
            #and run first production run. For n production steps
            #make n-1 scripts to run subsequent production runs
            
            sim_num = str(i+1)
            heat_script_name = ensemble_name + '_heat_and_prod1_md' + sim_num + '.sh' #name the heat/production script
            overwrite_test(working_dir,heat_script_name,overwrite)
            heat_script_fileobj = open(heat_script_name, 'a')
            heat_script_fileobj.write(template_str)
            
            
            #make names for files related to the heat solvent step
            heat_solv_mdout_name = ensemble_name + '_heat_solv_md' + sim_num + '.mdout'
            heat_solv_mdcrd_name = ensemble_name + '_heat_solv_md' + sim_num + '.mdcrd'
            heat_solv_rst_name = ensemble_name + '_heat_solv_md' + sim_num + '.rst'
            heat_solv_line = ('mpirun -np ' +  str(proc_per_sim) + ' pmemd.cuda ' + 
                              ovw + ' -i ' + heat_solv_mdin + ' -o ' + heat_solv_mdout_name +
                            ' -c ' + rlx_sys_rst + ' -p ' + prmtop + ' -x ' +
                            heat_solv_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                            ' -r ' + heat_solv_rst_name + '\n')
            heat_script_fileobj.write(heat_solv_line)
            
            #make names for files related to the heat system step
            #make a command to run the MD with appropriate input and output file names
            #for this step
            heat_sys_mdout_name = ensemble_name + '_heat_sys_md' + sim_num + '.mdout'
            heat_sys_mdcrd_name = ensemble_name + '_heat_sys_md' + sim_num + '.mdcrd'
            heat_sys_rst_name = ensemble_name + '_heat_sys_md' + sim_num + '.rst'
            heat_sys_line = ('mpirun -np ' +  str(proc_per_sim) + ' pmemd.cuda ' + 
                            ovw + ' -i ' + heat_sys_mdin + ' -o ' + heat_sys_mdout_name +
                            ' -c ' + heat_solv_rst_name + ' -p ' + prmtop + ' -x ' +
                            heat_sys_mdcrd_name + ' -ref ' + rlx_sys_rst + 
                            ' -r ' + heat_sys_rst_name + '\n')
            heat_script_fileobj.write(heat_sys_line)
            
            mem_file_name = ensemble_name + '_mem_file_md' + sim_num + '.txt'
            mem_file_obj = open(mem_file_name, 'a')
            mem_file_str1 = heat_solv_mdcrd_name + '\n' + heat_sys_mdcrd_name + '\n'
            mem_file_obj.write(mem_file_str1)
            
            
            for n in range(num_prod_steps):
                
                if n == 0:
                    
                    prod_base_name = ensemble_name + '_prod_step1_md' + sim_num
                    coord = heat_sys_rst_name #if first production step, set coordinate input
                                                #to the end of the heat system step
                    
                    
                else:
                    step_num = n+1
                    prod_base_name = ensemble_name + '_prod_step' + str(step_num) + '_md' + sim_num
                    coord = prod_rst_name #set coord value to the previous rst name
                    
                #give filenames for  the production related output files
                prod_mdout_name = prod_base_name + '.mdout'
                prod_rst_name = prod_base_name + '.rst'
                prod_mdcrd_name = prod_base_name + '.mdcrd'
                    
                prod_line = ('mpirun -np ' + str(proc_per_sim) + ' pmemd.cuda ' +
                               ovw + ' -i ' + prod_mdin + ' -o ' + prod_mdout_name +
                               ' -c ' + coord + ' -p ' + prmtop + ' -x ' +
                               prod_mdcrd_name + ' -r ' + prod_rst_name + '\n')
                
                mem_file_str2 = prod_mdcrd_name + '\n'
                mem_file_obj.write(mem_file_str2)
                #write line either to heat and production script or one of the n-1
                #production scripts
                if n == 0: #if this is our first production run command, keep it in the
                            #script with the heat solvent and heat sys commands
                    
                    heat_script_fileobj.write(prod_line)
                    heat_script_fileobj.close()
                    comm_str = 'chmod +x ' + heat_script_name
                    subprocess.run(comm_str.split())
                    
                else:
                    
                    prod_script_name = prod_base_name + '_run.sh'
                    prod_script_fileobj = open(prod_script_name, 'a')
                    prod_script_fileobj.write(template_str)
                    prod_script_fileobj.write(prod_line)
                    prod_script_fileobj.close()
                    comm_str = 'chmod +x ' + prod_script_name
                    subprocess.run(comm_str.split())
                    
                
            mem_file_obj.close()
            
    os.chdir(default_dir)

#==============================================================================
    
def prep_from_rosetta(pdb_file, #filepath to pdb file
                      output_dir, #output directory filepath
                      expt_name, #experiment name, serves as a name for inpcrd and prmtop files generated by tleap
                      ambdir, #directory containing all relevant modules and data to prepare inputs for MD and process MD output
                      box_length = None, #box length for TIP3BOX, in angstroms. Must enter a value if solvate is true
                      solvate = True, #do we solvate system? If so, 
                      add_ions = None, #If you want to add ions to the system, type the ion as you would in tleap as a string
                                         #for this input
                       forcefield = 'leaprc.ff14SB' #forcefield we want to use
                       ):
    
    #given a pdb file, and a specified output directory, make inpcrd and prmtop
    #in the directory.
    
    expt_name = output_dir.split('/')[-1]
    data_dir = os.path.join(ambdir,'data')
    aa_map_filepath = os.path.join(data_dir,'rosetta_amb_code_map_short.csv') #setup path to aa_map
    #the name of the aa_map is fixed, can edit if you want a different name
    #Suggested that one leaves the file and name as it is because the mapping
    #is critical to renaming noncanonical amino acids from rosetta nomenclature to amber nomenclature
    
    default_dir = os.getcwd()
    #print(pdb_names)
    
    
    if os.path.exists(output_dir):
        
        raise ValueError(output_dir + 'exists.')
        
    os.mkdir(output_dir)
    os.chdir(output_dir)
    
    #rename any non canonical residues, store the filename of the pdb with the renamed resdues
    #also, store the amber names of ncaas for residues whose names were changed.
    #these names will be used to find all the frcmod files we need to load in tleap
    
    renam_output = aa_rename_rosetta_to_amber(  pdb_filepath = pdb_file,
                                                     aa_mapping = aa_map_filepath,
                                                     output_folder = output_dir,
                                                     overwrite = True)
    renam_pdb_name = renam_output[0] #name of pdb with renamed 
    ncaa_names = renam_output[1] #list of ncaas used to find frcmod files
    
    #since we've changed directory to the subfolder, no need to specify full filepaths at this point
    
    #strip hydrogens, 
    # rename atoms, reduce structure, rename atoms to amber format again
    #for good measure. 
    strip_out_name = renam_pdb_name[0:-4] + '_stripH.pdb'
    strip_h2(renam_pdb_name, strip_out_name, overwrite = True)
    
    amber_out_name = strip_out_name[0:-4] + '_ambered.pdb'
    call_pdb4amber(input_pdb = strip_out_name,
                       output_pdb = amber_out_name,
                       overwrite = True)
    
    reduce_name = amber_out_name[0:-4] + '_reduced.pdb'
    call_reduce(input_pdb = amber_out_name,
                     output_pdb = reduce_name,
                     overwrite = True)
    
    amber_again_name = reduce_name[0:-4] + '_ambered.pdb'
    call_pdb4amber(input_pdb = reduce_name,
                       output_pdb = amber_again_name,
                       overwrite = True)
    
    #set up the filepath for input pdb for tleap
    leap_pdb_filepath = os.path.join(output_dir,amber_again_name)
    #setup path to ffncaa folder, ffncaa.in from selene.princeton.edu group
    ffncaa_folder = os.path.join(data_dir,'ffncaa')
    ffncaa_amberprep = os.path.join(ffncaa_folder,'ffncaa.in')
    
    frcmod_list = []
    
    #construct a list of frcmod files, based on the ncaas that were present
    #in the structure. frcmod files will all be loaded into tleap
    for n in range(0,len(ncaa_names),1):
        frcmod_file = ncaa_names[n] + '.frcmod'
        frcmod_list.append(os.path.join(ffncaa_folder,frcmod_file))
    
    (tleap_name, prmtop_name, inpcrd_name) = auto_tleap(input_pdb = leap_pdb_filepath,
                                                        ffncaa_dir = ffncaa_folder,
                                                        frcmod_list = frcmod_list, #filepath for frcmod file
                                                        amber_prep = ffncaa_amberprep, #filepath for amber prep file 
                                                        expt_name = expt_name, #just a name for the leap file, mdcrd, prmtop files
                                                        output_dir = output_dir, #Below this point, input variables/options not requiredto be specified
                                                        overwrite = True,
                                                        solvate = solvate,
                                                        solvatebox_length = box_length,
                                                        addions = add_ions, #generally, we will be adding ions to neutralize the system.
                                                                       # specify 'Na+' for sodium and 'Cl-' for chloride
                                                       forcefield = forcefield)
    
    #run tleap
    tleap_comm = 'tleap -s -f ' + tleap_name
    subprocess.run(tleap_comm.split(),) #run tleap and make mdcrd and prmtop files
    
    
    os.chdir(default_dir) #change back to default directory
    
    #return the names of the prmtop file and inpcrd file. \these will be initial inputs for pmemd,
    #and these names will be written in the initial relax script when pmemd called
    return(prmtop_name,inpcrd_name)            
        
#==============================================================================        
        
def prep_crd_denovo(ambdir, #directory containing all relevant modules and data to prepare inputs for MD and process MD output
                    crdfile, #amber coordinate file from H++ website, filename
                    prmtop, #amber prmtop file from H++ website, filename
                            
                            #Note that H++ is used to assign protonation state to
                            #histidines. Upload your 'raw' pdb to H++, calculate charge,
                            #and download the crdfile and prmtop files.
                    wdir, #working directory, where all input coordinate and prmtop files are stored,
                            #and where all outputs will be put
                    expt_name, #just a name that will be given to the inpcrd and prmtop files output by tleap
                    solvate = True, #Do we solvate?
                    add_ions = None, #generally, we will be adding ions to neutralize the system.
                                    # specify 'Na+' for sodium and 'Cl-' for chloride
                    box_length = None,
                    forcefield = 'leaprc.ff14SB' #which amber forcefield to use. string must be exactly as you'd type the forcefield filename in tleap
                    ):
    
    #Motivation: To prepare entirely new (unmutated) structures for MD,
    # we will put a pdb into H++, obtain a structure with correct protonation state
    # given a pH of 6.5, and download the resultant amber crd and prmtop files.
    # Once we have these files, we shall use this function to do the necessary
    # preprocessing and produce amber ready inpcrd and prmtop files from tleap
    
    #set box_length to float if we have a box length
    if type(box_length) == int:
        box_length = float(box_length)
    #if we're solvating, there needs to be a box length
    if (solvate == True) & (type(box_length) != float):
        
        raise ValueError('If solvate is True, box length must be specified as a number')
        
    default_dir = os.getcwd()
    os.chdir(wdir) #change to working directory
    
    ambpdb_name = crdfile[0:-4] + '.pdb' #give name to pdb file that will be input to tleap
    
    if os.path.exists(ambpdb_name):
        
        raise ValueError(ambpdb_name + 'already exists. remove file from folder or rename')
    
    #make the pdb    
    command_str = 'ambpdb -c ' + crdfile + ' -p ' + prmtop
    ambpdb_obj = open(ambpdb_name, 'a')
    
    subprocess.run(command_str.split(), stdout = ambpdb_obj)
    ambpdb_obj.close()
    
    #ffncaa_folder is currently a required input, as is the prepin file, for auto_tleap
    #this shouldn't affect anything because we're not using any non-canonical residues
    #note: if we do come across the need to use non-canonical residues when
    #'prepping-denovo', we need to put in an option for an frcmod_list
    
    data_dir = os.path.join(ambdir,'data')
    ffncaa_folder = os.path.join(data_dir,'ffncaa')
    ffncaa_amberprep = os.path.join(ffncaa_folder,'ffncaa.in')
    
    #make the tleap input file
    (tleap_name,prmtop_name,inpcrd_name) = auto_tleap(  input_pdb = ambpdb_name,
                                                        ffncaa_dir = ffncaa_folder,
                                                        frcmod_list = None, #filepath for frcmod file
                                                        amber_prep = ffncaa_amberprep, #filepath for amber prep file 
                                                        expt_name = expt_name, #just a name for the leap file, mdcrd, prmtop files
                                                        output_dir = wdir, #Below this point, input variables/options not requiredto be specified
                                                        overwrite = True,
                                                        solvate = solvate,
                                                        solvatebox_length = box_length,
                                                        addions = add_ions, #generally, we will be adding ions to neutralize the system.
                                                                       # specify 'Na+' for sodium and 'CL-' for chloride
                                                        forcefield = forcefield)
    
    #run tleap
    tleap_comm = 'tleap -s -f ' + tleap_name
    subprocess.run(tleap_comm.split()) #run tleap and make mdcrd and prmtop files
    
    os.chdir(default_dir) #change back to default directory
    
     #return the names of the prmtop file and inpcrd file. \these will be initial inputs for pmemd,
    #and these names will be written in the initial relax script when pmemd called
    return(prmtop_name, inpcrd_name)

#==============================================================================
    
def make_amber_input(wdir, #working directory where you're putting all inputs for md, including mdin files, groupfiles, mdcrd, prmtop
                     ambdir, #amber directory, so we can find any needed data (i.e. mdin file templates)
                     expt_name, #name of the experiment
                     num_residues = None, #
                     rlx_mode = 'default',
                     eq_mode = 'default',
                     prod_mode = 'default'):
    
    #Purpose: given a list of template input files, make copies of template files
    #with appropriate information (namely the residues to appy restraints to, where applicable),
    #and write them to the working directory containing inputs for an MD simulation
    
    #Note that one can add different modes for rlx_mode, eq_mode, and prod_mode
    #by editing this function and adding 'elif' statements to allow the condition
    #of another mode. Any mode must specify a condition that points to which
    #files (stored in ambdir/data/md_input_templates).
    
    #Also, when giving a name for a mode, make sure that it does not have any '_'
    #characters in it. This function assumes that each template file is prefixed
    #by <modename>_your_protocol.in. Relax solvent protocol should have _rlx_solv_imin.in
    #following said prefix, and similar rules apply for other protocol filenames.
    
    #Whatever you do, unless you fully understand the organization of the
    #amber/rosetta pipeline, do NOT remove or change the default files for the below code.
    #If you want, you can go in the 
    
    template_dir = os.path.join(ambdir,'data/md_input_templates')
    

    if rlx_mode == 'default': #If statement to specify relax mode
        
        if type(num_residues) != type(int(5)):
            raise ValueError('num_residues must be entered as an integer if default settings used\
 for relaxation')
        
        imin_solv_protocol = 'default_rlx_solv_imin.in'
        imin_sys_protocol = 'default_rlx_sys_imin.in'
    
    else:
        
        raise ValueError('rlx_mode must be specified')
        
    if eq_mode == 'default': #If statement to specify heat equilibration mode

        if type(num_residues) != type(int(5)):
            
            raise ValueError('num_residues must be entered as an integer if default settings used\
 for heat equilibration')
            
        heat_solv_protocol = 'default_heat_solv_md.in'
        heat_sys_protocol = 'default_heat_sys_md.in'
        
    else:
        
        raise ValueError('eq_mode must be specified')
        
    if prod_mode == 'default': #If statement to specify production mode
            
        prod_protocol = 'default_prod_md.in'
        
    elif prod_mode == '10ns':
    
        prod_protocol = '10ns_prod_md.in'    
    
    else:
        
        raise ValueError('prod_mode must be specified')
        
        
    file_list = [imin_solv_protocol, imin_sys_protocol, heat_solv_protocol,
                heat_sys_protocol, prod_protocol]
    
    out_file_list = [] #to contain filenames for the copy files, in same order
                        # of the corresponding originals in file_list.
                        #these files will be used by prep4md to construct
                        #group files and bash scripts to call amber to carry out
                        #the minimization, relaxation, and simulation steps
    
    for i in range(0,len(file_list),1):
        
        file_name = file_list[i]
        filepath = os.path.join(template_dir, file_name)
        fileObj = open(filepath)
        
        file_pattern = '_[\S]*.[\S]*' #find the protocol name using a regular expression
        match = re.search(file_pattern,file_name)
        protocol_name = match.group(0)
        
        newFile = expt_name + protocol_name #name of the copied input file
        newFilePath = os.path.join(wdir,newFile)
        
        if os.path.exists(newFilePath):
            raise ValueError(newFilePath + ' already exists')
        
        newFileObj = open(newFilePath, 'a')
        
        if type(num_residues) == type(int(5)):
            
            start_res = '1'
            end_res = str(num_residues)
        
        for line in fileObj:
        
            if type(num_residues) == type(int(5)):
                
                if line[0:4] == 'RES\n':
                    
                    newLine = line[0:3] + ' ' + start_res + ' ' + end_res + '\n'
                    
                else:
                    
                    newLine = line
                    
            else:
                
                newLine = line
                
            newFileObj.write(newLine)
        
        fileObj.close()
        newFileObj.close()
        
        out_file_list.append(newFile) #append filename to list of files to return
        
    return(out_file_list)

#==============================================================================

def prep_md_inputs(mode, #are we taking pdbs from rosetta or starting de novo with inpcrd and prmtop files from H++? 
                   ambdir,
                   num_simulations,
                   num_prod_steps,
                   proc_per_sim,
                   script_template = 'script_template.sh',
                   input_mode = 'mpi-cpu', #Are we preparing inputs for simlations run on cpus, or gpu on amber?
                                           #alternatively, can specify None if you don't want amber inputs aside from
                                           #an md ready pair of prmtop and inpcrd files
                   num_residues = None, #required to be of type int if using any of default protocols
                   rlx_mode = 'default',
                   eq_mode = 'default',
                   prod_mode = 'default',
                   wdir = None, #required if mode = denovo
                   crdfile = None, #required if mode = denovo, crdfile from H++ website
                   prmtop = None, #required if mode = denovo, topology file from H++ website
                   expt_name = None, #required if mode = denovo, variable will
                   #be specified automatically if mode = rosetta
                   output_dir = None, #required if mode = rosetta, 
                   #must be a string giving filepath to soon-to-exist folder 
                   #where subdirectories will contain all input files to run an md
                   #for a given rosetta generated pdb (one subfolder made for each)
                   #pdb in the pdb_folder from which we will be generating md inputs
                   pdb_folder = None, #required if mode = rosetta, 
                   #must be a string giving filepath to folder 
                   #containing pdbs genearted by rosetta
                   solvate = True, #if true, use explicit solvent with periodic boundaries
                   add_ions = None, #not strictly required, but needed if protein has a net charge
                   box_length = None, #requred if solvate = True
                   forcefield = 'leaprc.ff14SB'):

    #overview: The purpose of this function is to 1) take a crd file and top file
    #generated by H++ and generate all the necesary input files and bash scripts
    #to run am MD on scinet, or 2) given a folder of pdb files generated by rosetta, and a name for an
    #output directory, make a subdirectory of the output directory for each pdb file
    #containing all the necessary input files and bash scripts to run an md for
    #the given pdb structure. The first task pertains to generating inputs for an MD
    #for a structure that has not yet been mutated via rosetta, but has been
    #given a charge via H++. The second task pertains to generating MD related inputs
    #and scripts for rosetta generated mutants
                        
    if mode == 'denovo':
        
        
        #make the amber input files in the working directory
        input_file_list = make_amber_input(  wdir, #working directory where you're putting all inputs 
                                               #for md, including mdin files, groupfiles, mdcrd, prmtop
                                             ambdir, #amber directory, so we can find any needed data (i.e. mdin file templates)
                                             expt_name, #name of the experiment
                                             num_residues = num_residues, #required to be an int if using default protocol
                                             rlx_mode = rlx_mode,
                                             eq_mode = eq_mode,
                                             prod_mode = prod_mode)
        
        rlx_solv_file = input_file_list[0]
        rlx_sys_file = input_file_list[1]
        heat_solv_file = input_file_list[2]
        heat_sys_file = input_file_list[3]
        prod_file = input_file_list[4]
        
        #make the inpcrd and prmtop files
        (prmtop_name, inpcrd_name) = prep_crd_denovo(   ambdir, 
                                                        crdfile, 
                                                        prmtop, #this refers to the input prmtop file, not the one that gets used in MD
                                                        wdir, #working directory
                                                        expt_name, #experiment name
                                                        solvate = True,
                                                        add_ions = add_ions, 
                                                        box_length = box_length,
                                                        forcefield = forcefield)
        
        #make the required groupfiles and bash scripts
        
        prep_for_MD(ambdir = ambdir,
                    working_dir = wdir, #all inputs and outputs expected to be in same folder
                    ensemble_name = expt_name,  #any name you want for the ensemble. serves as a prefix to all outputs
                    inpcrd = inpcrd_name, #coordinate file generated by tleap from pdb
                    prmtop = prmtop_name, #parameter/topology file
                    rlx_solv_imin = rlx_solv_file,
                    rlx_sys_imin = rlx_sys_file,
                    heat_solv_mdin = heat_solv_file,
                    heat_sys_mdin = heat_sys_file, 
                    prod_mdin = prod_file, 
                    num_simulations = num_simulations,
                    proc_per_sim = proc_per_sim, #processors per simulation. used to calculate number of processors needed
                                    #should be a multiple of 8 #entered as a string in format 'hh:mm:ss'
                    num_prod_steps = num_prod_steps,
                    mode = input_mode,
                    script_template = script_template, #template for md bash scripts to be written
                    overwrite = True)
        
    elif mode == 'rosetta':
        
        if os.path.exists(output_dir):
            
            raise ValueError(output_dir + ' already exists')
        
        #make the output directory, and open a file object for a mapping file
        os.mkdir(output_dir)    
        pdb_list = os.listdir(pdb_folder)
        mapping_file_name = output_dir.split('/')[-1] + '_mut_map.txt'
        mapfile_obj = open(os.path.join(output_dir,mapping_file_name),'a')
        
        #write a header for the mapfile
        mapfile_first_line = 'source_pdb\texpt_name\n'
        mapfile_obj.write(mapfile_first_line)
        
        for i in range(0,len(pdb_list),1):
            
            #set up names for experiment, subfolder/experiment folder
            pdb_name = pdb_list[i]
            expt_name = output_dir.split('/')[-1] + '_mut_' + str(i)
            subfolder_path = os.path.join(output_dir,expt_name)
            pdb_filepath = os.path.join(pdb_folder,pdb_name)
            
            #create an entry in mapfile
            mapfile_line = pdb_name + '\t' + expt_name + '\n'
            mapfile_obj.write(mapfile_line)
            
            
            (prmtop_name,inpcrd_name) = prep_from_rosetta(pdb_file = pdb_filepath,
                                                          output_dir = subfolder_path,
                                                          expt_name = expt_name,
                                                          ambdir = ambdir, 
                                                          box_length = box_length,
                                                          solvate = True,
                                                          add_ions = add_ions,
                                                          forcefield = forcefield)
            
            wdir = subfolder_path

            #make the amber input files in the working directory
            input_file_list = make_amber_input(  wdir, #working directory where you're putting all inputs 
                                                   #for md, including mdin files, groupfiles, mdcrd, prmtop
                                                 ambdir, #amber directory, so we can find any needed data (i.e. mdin file templates)
                                                 expt_name, #name of the experiment
                                                 num_residues = num_residues, #required to be an int if using default protocol
                                                 rlx_mode = rlx_mode,
                                                 eq_mode = eq_mode,
                                                 prod_mode = prod_mode)
            
            rlx_solv_file = input_file_list[0]
            rlx_sys_file = input_file_list[1]
            heat_solv_file = input_file_list[2]
            heat_sys_file = input_file_list[3]
            prod_file = input_file_list[4]
            
            prep_for_MD(ambdir = ambdir,
                    working_dir = wdir, #all inputs and outputs expected to be in same folder
                    ensemble_name = expt_name,  #any name you want for the ensemble. serves as a prefix to all outputs
                    inpcrd = inpcrd_name, #coordinate file generated by tleap from pdb
                    prmtop = prmtop_name, #parameter/topology file
                    rlx_solv_imin = rlx_solv_file,
                    rlx_sys_imin = rlx_sys_file,
                    heat_solv_mdin = heat_solv_file,
                    heat_sys_mdin = heat_sys_file, 
                    prod_mdin = prod_file, 
                    num_simulations = num_simulations,
                    proc_per_sim = proc_per_sim, #processors per simulation. used to calculate number of processors needed
                    num_prod_steps = num_prod_steps,
                    mode = input_mode,
                    script_template = script_template, #template for md bash scripts to be written
                    overwrite = True)
            
        
    else:
        
        raise ValueError('Mode ' + mode +' not recognized. Expecting \'denovo\' or \'rosetta\'')
                
    
#if __name__ == '__main__':
    
#    amber_directory = os.path.join(os.getcwd(),'../')
#    working_directory = '/home/kimlab2/owenwhitley/amber_projects/5q0k_test_MD_prep'
#    prep_md_inputs(mode = 'denovo', #are we taking pdbs from rosetta or a 'new' pdb that hasn't been mutated?
#                   ambdir = amber_directory,
#                   num_simulations = 8,
#                   proc_per_sim = 16,
#                   wall_time = '20:00:00',
#                   script_template = 'script_template.sh',
#                   num_residues = 244, #required to be of type int if using any of default protocols
#                   rlx_mode = 'default',
#                   eq_mode = 'default',
#                   prod_mode = 'default',
#                   wdir = working_directory, #required if mode = denovo
#                   crdfile = '0.15_80_10_pH6.5_5q0k_fill.B99990001.crd', #required if mode = denovo, crdfile from H++ website
#                   prmtop = '0.15_80_10_pH6.5_5q0k_fill.B99990001.top', #required if mode = denovo, topology file from H++ website
#                   expt_name = '5q0k_test_MD_prep', #required if mode = denovo, variable will
#                   #be specified automatically if mode = rosetta
#                   output_dir = None, #required if mode = rosetta, 
#                   #must be a string giving filepath to soon-to-exist folder 
#                   #where subdirectories will contain all input files to run an md
#                   #for a given rosetta generated pdb (one subfolder made for each)
#                   #pdb in the pdb_folder from which we will be generating md inputs
#                   pdb_folder = None, #required if mode = rosetta, 
#                   #must be a string giving filepath to folder 
#                   #containing pdbs genearted by rosetta
#                   solvate = True, #if true, use explicit solvent with periodic boundaries
#                   add_ions = 'Na+', #not strictly required, but needed if protein has a net charge
#                   box_length = 11, #requred if solvate = True
#                   forcefield = 'leaprc.ff14SB')    
#  
#    amber_directory = os.path.join(os.getcwd(),'../')
#    output_dir = '/home/kimlab2/owenwhitley/amber_projects/5q0k_071417_ensemble_muts'
#    pdb_folder='/home/kimlab2/owenwhitley/rosetta_project/5q0k_071417_ensemble/for_amber'
#    prep_md_inputs(mode = 'rosetta', #are we taking pdbs from rosetta or 
#                   ambdir = amber_directory,
#                   num_simulations = 8,
#                   proc_per_sim = 16,
#                   script_template = 'script_template.sh',
#                   num_residues = 244, #required to be of type int if using any of default protocols
#                   rlx_mode = 'default',
#                   eq_mode = 'default',
#                   prod_mode = 'default',
#                   wdir = None, #required if mode = denovo
#                   crdfile = None, #required if mode = denovo, crdfile from H++ website
#                   prmtop = None, #required if mode = denovo, topology file from H++ website
#                   expt_name = None, #required if mode = denovo, variable will
#                   #be specified automatically if mode = rosetta
#                   output_dir = output_dir, #required if mode = rosetta, 
#                   #must be a string giving filepath to soon-to-exist folder 
#                   #where subdirectories will contain all input files to run an md
#                   #for a given rosetta generated pdb (one subfolder made for each)
#                   #pdb in the pdb_folder from which we will be generating md inputs
#                   pdb_folder = pdb_folder, #required if mode = rosetta, 
#                   #must be a string giving filepath to folder 
#                   #containing pdbs genearted by rosetta
#                   solvate = True, #if true, use explicit solvent with periodic boundaries
#                   add_ions = 'Na+', #not strictly required, but needed if protein has a net charge
#                   box_length = 10, #requred if solvate = True
#                   forcefield = 'leaprc.ff14SB')  