#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 14:58:16 2017

@author: owenwhitley
"""
import pytraj as pt
import pytraj_utils as pyu
import os
import subprocess
from matplotlib import pyplot as plt

#NOTE: THIS MODULE MAKES USE OF PYTRAJ TRAJECTORY OBJECTS, AND AS SUCH,
# AT THE TIME OF WRITING, PYTRAJ IS NOT SUPPORTED ON PYTHON 3.6.
# ANY SCRIPTS MAKING USE OF THE FUNCTIONS LISTED BELOW MUST BE RUN ON PYTHON3.5

def interchain_dist(traj,ligand_range,target_range, dist_filter = True, thresh = 8.0):
    #traj: pytraj trajectory, isngle frame, contains all atoms and same topology as
    #structures in md simulation
    #ligand_range: list of form [start,end], residue range for ligand
    #target_range: list of form[start,end ]
    #dist_filter: logical, do we filter the output list by pairwise inter-residue distance (i.e. distance from target residue to ligand residue)
    #thresh: maximum distance for residue pairs included in the list of target residues, in Angstroms
#    traj = pt.load(input_pdb)

#   PURPOSE: for an input trajectory, calculate pairwise distance for each ligand residue / target residue pair.

#   general algorithm: for each ligand residue, calculate pairwise distance with each
#                       target residue. If distance fileter is on, and distance
#                       for a given pair of residues is greater than threshold,
#                       skip residue pair. otherwise, append the residue (really the residue numeber)
#                       for the ligand and/or that for the target molecule to the
#                       residue list, if not already in the list.

#                       return a list of residue indices (which residues to keep)
#                       which, if dist_filter was on, will only include residues
#                       that were within teh distance threshold. Also return a list
#                       of ligand/target residue pairs and associated interchain
#                       distance


    pair_list = [] #to contain, in integer form, [ligand_residue,target_residue, pairwise_dist]
    res_list = [] # to contain all residues with interchain distance of < thresh, if dist_filter is True
    
    for i in range(ligand_range[0],ligand_range[1],1):
        
        ligand_res = i
        
        for n in range(target_range[0],target_range[1],1):
            
            target_res = n
            pair_mask = ':' + str(ligand_res) + ' :' + str(target_res)
            pairwise_dist = pt.distance(traj,mask = pair_mask)
            
            pair_list.append([ligand_res,target_res,pairwise_dist[0]])
            
            if (dist_filter == True) & (pairwise_dist > thresh):
                continue
            
            if ligand_res not in res_list:
                res_list.append(ligand_res)
                
            if target_res not in res_list:
                res_list.append(target_res)
            
            
    pair_list.sort(key = lambda x:x[2]) #sort pair_list by pairwise distance
    
    return(res_list,pair_list)

def get_interface(input_pdb, #filepath to input pdb 
                  res_list, #list of integers, output from interchain_dist.
                  output_pdb):
    
    #given an input pdb and a list of residue indices, filter out residues
    #whose index not in the list. To be used in conjunction with interchain_dist
    #to filter for residues that occur at the interface of a ligand/target interaction
    #The residues that pass the test will be written into the output pdb file.
    #Alternatively, it is possible to turn thresholding off 
    
    fileobj = open(input_pdb)
    out_file = open(output_pdb, 'a')
    
    for line in fileobj:
        
        if (line[0:4] == 'ATOM') | (line[0:3] == 'TER'): #If at an atom or TER line
            
            if int(line[22:26]) in res_list: #if line is for residue whose serial number is in our list
                
                out_file.write(line) #write the line
                
    fileobj.close()
    out_file.close()
                
def mmtsb_kclust(pdb_identifier, #string that on linux should capture all of the pdbs we wish to put through clustering
                 #e.g. ./pdbs4clust/* if all pdbs within that folder
                 output_file, #name of cluster.pl output file
                 radius = 1.0, #cluster radius
                 wdir = os.getcwd()):
    
    #This makes a script capable of running cluster.pl under the kclust protocol.
    #however, the subprocess call command does not make the cluster.pl script run as hoped.
    #This function should be modified or discarded 
    
    default_dir = os.getcwd()
    os.chdir(wdir)
    
    temp_bash_name =  'temp.sh'
    
    if os.path.exists(temp_bash_name):
        os.remove(temp_bash_name)
    
    fileobj = open(temp_bash_name, 'a')
    fileobj.write('#!/bin/bash\n')
    fileobj.write('cluster.pl -kclust -iterate  -radius 0.75 -mode rmsd -centroid -pdb '
  + pdb_identifier + ' > ' + output_file)
    
    command_1 = 'chmod +x temp.sh'
    
    subprocess.run(command_1.split())
    subprocess.run(['bash','temp.sh'])
#    subprocess.run(command_3.split())
    
    os.chdir(default_dir)
    
def split_pdb(input_file, #filepath to input pdb. will contain multiple 'models' corresponding to frames of
                            #an MD trajectory
              num_chains, #number of chains in structure
              output_name_root#simply a prefix for a filename. enter as a string
              ):
 
#Overview: read file, writing lines to new file until you reach the num_chains
#TER line. Change filename for output and repeat process

#Note that this is currently configured for pdbs being output by pytraj
#openmm appears to give slightly different format, so either this function will need 
#to be changed, or a parallel function accomplishing the same thing for
#openmm generated multi-frame pdbs
       
        i = 0
        n = 0
        fileobj = open(input_file, 'r')
        cutoff = False
        
        for line in fileobj:
            
              
                
        #    if 'MODEL' in line:
            if ((i == 0) | (cutoff == True)) & (line[0:4] == 'ATOM'):
                i = i + 1
                newFileName = output_name_root + str(i) + '.pdb'
                newFileObj = open(newFileName, 'a' )
                newFileObj.write(line)
                cutoff = False
                
        #    elif 'ENDMODL' in line:
                
        
                
            elif i > 0:
                
                if 'TER' in line[0:3]:
                    
                    n = n + 1  
                    
                    if (n % num_chains) == 0: 
                
                        newFileObj.write(line)
                        newFileObj.close()
                        cutoff = True
                        
                    else:
                        
                        newFileObj.write(line)
                        
                elif 'END' in line[0:3]:
                    continue
                        
                else:
                    
                    newFileObj.write(line)
            
def make_pdbs(input_dir, #string, path to input directory
              top, #name, not path, of topology file, located in input_dir
              output_dir, #string, path to output directory
              prod_frames, #the number of frames corresponding to the production phase of MD
                          #Will make pdb files for the last (prod_frames) frames of entire trajectory
              num_chains, #number of chains in structure, integer
              ref, #reference structure for calculating RMSD. expected to be rst file for system minimization (structure just prior to MD)
              ligand_range, #range of residues #s in structure corresponding to ligand, enter as a list of two integers,
                          #with the lower number corresponding to the first residue and the higher number corresponding to the last
              target_range, #same as ligand_range, but corresponding to the range of residues in structure corresponding to
                          #the last 
              mask = '!:WAT,CL-,Na+', #mask for pytraj to select residues/atoms for the trajectory that will be written to output
              mass = True, #mass weight RMSD?
              rmsd_lim = 5.0, #If rmsd at any frame every goes above this value, do not write pdb to output
              interface_thresh = 8.0 #threshold distance between residues to define interface, unit = Angstroms
              ):
    
    #Note: this will not work as is with openmm generated pdbs. Should make an 
    #openmm mode to take openmm pdb (pdb for openmm trajectory) as input, filter out solvent and ions,
    #calculate pairwise distances and define ligand/target interface, generate
    #interface and full pdb structures. Then we can run clustering protocol upon
    #the interface pdbs.
    
    #overview: in the input directory, you should have a combination of 
    # mdcrd files for equilibration and production steps of MD,
    # for each simulation run, as well as mem_files, which map
    # the mdcrd files of each step for a particular simulation
    # and have the named files in the order of the steps that occured
    # i.e. solvent equilibration mdcrd comes before system equilibration mdcrd
    # which comes before production mdcrd.
    
    #For every MD simulation, we'd like to load the appropraite mdcrd files into
    # 1 trajectory, calculate rmsd for it, decide whether or not to keep it
    #based on whether or not it passes a threshold for RMSD for every frame
    #If we keep the coordinates for a simulation, we take the frames that
    #pertain to the production phase of MD (frames [-prod_frames:len(total trajectory)]), 
    #and write them all to pdb file in subdirectory of output directory. Make pdb files
    #split by frame, and remove the original multi frame pdb.
    
    #Expected that ref will be the rst file for the system minimization.
    
    
    if os.path.exists(output_dir) == False:
        
        os.mkdir(output_dir)
        pdb_frame_dir = os.path.join(output_dir,'md_frame_pdbs')
        os.mkdir(pdb_frame_dir)
        interface_dir = os.path.join(output_dir,'interface_pdbs')
        os.mkdir(interface_dir)
        rmsd_calc_dir = os.path.join(output_dir,'rmsd_calculations')
        os.mkdir(rmsd_calc_dir)
        clust_dir = os.path.join(output_dir,'clustering')
        os.mkdir(clust_dir)
        
    else:
        
        raise ValueError(output_dir + ' already exists, please pick another name for output_dir')
        
    dir_list = os.listdir(input_dir)
    
    mem_file_list = []

    for i in range(len(dir_list)):
        
        if 'mem_file_md' in dir_list[i]:
            mem_file_list.append(dir_list[i])
            
            
    for n in range(len(mem_file_list)):
        
        #get mem_file
        mem_file = mem_file_list[n]
        #get sequence of mdcrd files
        filelist = pyu.obtain_mdcrd_seq(working_dir = input_dir, mem_file = mem_file)
        #calculate rmsd for all frames across entire sequnce, relative to first frame
        
        
        (rmsd_array,traj) = pyu.stitched_rmsd(working_dir = input_dir, file_list = filelist, 
                                        top = top, ref = ref, mask = mask, mass = mass)
        #obtain a boolean conditioned on the presence of any above threshold
        #rmsd values
        keep_mdcrd = pyu.filter_rmsd(rmsd_array = rmsd_array, limit = rmsd_lim, mode = 'lt')
        
        if keep_mdcrd:
            
            #If we're keeping the trajectory, get the production frames and write them to a pdb
            
            prod_traj = traj[-prod_frames:]
            prod_mdcrd_name = filelist[2]
            output_pdb_name = prod_mdcrd_name[0:-6] + '.pdb'
            output_pdb_filepath = os.path.join(pdb_frame_dir, output_pdb_name)
            pt.write_traj(output_pdb_filepath, prod_traj)
            
            pdb_split_root_name = output_pdb_filepath[0:-4] + '_'
            split_pdb(output_pdb_filepath,num_chains,pdb_split_root_name)
            os.remove(output_pdb_filepath)
            
        
        #make a plot, give it a name corresponding to the md run, and save figure    
        rmsd_plot = plt.plot(rmsd_array)
        rmsd_plot_name = mem_file[0:-16] + 'rmsd' + mem_file[-8:-4] + '.png'
        rmsd_plot_path = os.path.join(rmsd_calc_dir,rmsd_plot_name)
        plt.savefig(rmsd_plot_path)
        plt.clf()
        
        #save the info to a file
        rmsd_filename = rmsd_plot_name[0:-4] + '.txt'
        rmsd_filepath = os.path.join(rmsd_calc_dir, rmsd_filename)
        rmsd_file = open(rmsd_filepath,'a')
        
        for m in range(len(rmsd_array) + 1):
            
            if m == 0:
                newLine = 'mass = ' + str(mass) + '\n mask = ' + str(mask) + '\n'
            else:
                newLine = str(m) + '\t' + str(rmsd_array[m-1]) + '\n'
            rmsd_file.write(newLine)
            
            if m == (len(rmsd_array)):
                
                rmsd_file.close()
        
    #Load the reference structure, define the interface, and generate interface
    #filtered pdbs in the interface directory        
    refpath = os.path.join(input_dir, ref)
    top_path = os.path.join(input_dir,top)
    reftraj = pt.load(refpath, top = top_path, mask = mask)
    (res_list, pair_list) = interchain_dist(reftraj, ligand_range,target_range, dist_filter = True, thresh = interface_thresh)
    
    list_pdb_dir = os.listdir(pdb_frame_dir)
    
    for i in range(0,len(list_pdb_dir),1):
        
        pdb_name = list_pdb_dir[i]
        pdb_path = os.path.join(pdb_frame_dir,pdb_name)
        output_pdb_name = pdb_name[0:-4] + '_interface.pdb'
        output_pdb_path = os.path.join(interface_dir, output_pdb_name)
        
        get_interface(pdb_path, res_list, output_pdb_path) #generate interface filtered pdb
        
    
        
            
if __name__ == '__main__':

#Just used this as a space for testing code

#    input_pdb = '5q0k_imin_sys.pdb'
#    ligand_range = [232,244]
#    target_range = [1,231]
#    outputs = interchain_dist(input_pdb,ligand_range,target_range, dist_filter = True, thresh = 8.0)    
#
#    res_list = outputs[0]
#    output_pdb = '5q0k_imin_sys_interface.pdb'
#    
#    if os.path.exists(output_pdb):
#        os.remove(output_pdb)
#        
#    get_interface(input_pdb,res_list,output_pdb)    
#    
#    pdb_list = os.listdir('PDBs')
#    
#    for i in range(0,len(pdb_list),1):
#        
#        filename = os.path.join('PDBs',pdb_list[i])
#        pdb_name = pdb_list[i]
#        out_file_name = os.path.join('interface_pdbs',(pdb_name[0:-4] + '_interface.pdb'))
#        get_interface(filename,res_list,out_file_name) 

#    input_dir = '/home/kimlab2/owenwhitley/amber_projects/5q0k_H++/071417_ensemble'
#    top = '5q0k_prepped.prmtop'
#    output_dir = '/home/kimlab2/owenwhitley/amber_projects/5q0k_H++/071417_ensemble/analysis_test'
#    prod_frames = 4500
#    num_chains = 2
#    ref = '../5q0t_rlx_sys_imin.rst'
#    ligand_range = [232,244]
#    target_range = [1,231]
#    make_pdbs(input_dir, top, 
#              output_dir, prod_frames, 
#              num_chains, ref,
#              ligand_range, target_range,
#              mask = '!:WAT,CL-,Na+', 
#              mass = True, rmsd_lim = 5.0,
#              interface_thresh = 8.0)   
#    wdir = '/home/kimlab2/owenwhitley/amber_projects/5q0k_H++/071417_ensemble/interface_cluster'
#    mmtsb_kclust('interface_pdbs/*.pdb', 'output_file_7.txt', radius = 1.0, wdir = wdir)

    print('finished')    