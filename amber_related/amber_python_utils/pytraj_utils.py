#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 14 12:09:40 2017

@author: owenwhitley
"""
import pytraj as pt
import os
import numpy

def obtain_mdcrd_seq(working_dir, #working directory with all your md inputs and outputs
                     mem_file #essentially a text file with each line containing
                             #a filename for a trajectory file corresponding to a stage of
                             #a given simulation. First line contains heat solvent step,
                             #2nd line contains heat system step, third and subsequent lines
                             #correspond to production step trajectory files (mdcrd)
                     ):
    
    #purpose: given a memory file containing information on the order and names of
    #mdcrd files for a sequence of simulations (i.e. simulations that had inputs
    # and outputs interfacing, such as heat solvent step, heat system step, production step)
    # return the ordered list in the form of a python list
    
    default_dir = os.getcwd()
    os.chdir(working_dir)
    
    mem_file_obj = open(mem_file, 'r')
    
    mem_file_str = mem_file_obj.read()
    mem_file_str_split = mem_file_str.split('\n')
    file_list = mem_file_str_split[0:(len(mem_file_str_split)-1)]
    
    os.chdir(default_dir)    
    return(file_list)
    

def stitched_rmsd(working_dir,#working directory with all your md inputs and outputs
                  file_list,  #list, containing filenames. should have file_list output from obtain_mdcrd_seq
                  top, #topology file name 
                  ref = 0, #reference. can be an integer (0 is first frame of trjectory), or a trajectory file
                  mask = '', #amber style mask for filtering atoms that are included in trajectory
                  mass = False #mass weight RMSD?
                  ):
    
    #Purpose: given an ordered list of mdcrd files, stich them into a single trajectory in pytraj,
    #calculate rmsd return the resulting rmsd array as a numpy array. Also return
    #the trajectory of the md simulation
    
    #If we have a set of coordinates we'd like to use as a reference (an rst file)
    #we load the file as a trajectory, using the same topology as for the md simulation
    #mdcrd files. For the purposes of our pipeline, we will be using
    #the system minimization rst file as our reference, since it is the structure
    #from which all md simulations will diverge. We do this so that all frames
    #for all md structures used in clustering will be aligned by rmsd fit,
    #so that we don't get weird results from clustering (e.g. two very differnt
    #conformations look very similar because they are 'upside down' relative to one another)
    
    #It should be noted that, if a reference file is used, it is expected that it contain the same atoms
    #and share topology with the md structures
    
    #Note that mask can be specified to filter for types of atoms/residues defined by amber
    #must also provide a topology file, located within the working directory,
    #otherwise provide a relative path to the topology file
    
    default_dir = os.getcwd()
    os.chdir(working_dir)
    traj = pt.load(file_list, top = top, mask = mask)
    
    if type(ref) == type('string'):
        
        reftraj = pt.load(ref, top = top, mask = mask )
        ref = reftraj[0]
    
    rmsd_array = pt.rmsd(traj = traj,ref = ref, mass = mass)
    
    os.chdir(default_dir)
    return(rmsd_array,traj)
    
def filter_rmsd(rmsd_array, limit, mode = 'lt'):
    
    #purpose: go through the rmsd_array, and depending on the mode,
    #evaluate if all numbers are less than, or less than or equal to,
    #the stated rmsd limit
    
    cond = True
    
    for i in range(0,len(rmsd_array),1):

        if mode == 'lt':
            
            if rmsd_array[i] >= limit:
                
                cond = False
                
        if mode == 'lteq':
            
            if rmsd_array[i] > limit:
                
                cond = False
    
    return(cond)        