#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 14:11:33 2017

@author: owenwhitley
"""
import os
import re
import subprocess

def clust_parser(input_file, #should be an output file from cluster.pl script, which ran using kclust option
                 overwrite = False,
                 wdir = os.getcwd()):
    #idea: take cluster output file from cluster.pl script, find the lowest level
    #clusters (i.e. 0 subclusters), order the elements of each cluster by rmsd relative to centroid, and return
    #ordered cluster lists in one file/variable and a mapping of cluster:closest_to_centroid
    #in text file form and returnable as a variable. Note that the latter file contains
    #as well the count of structures and fraction of total population for each cluster
    
    default_dir = os.getcwd()
    os.chdir(wdir)
    
    fileobj = open(input_file)
    cont = False
    clust_index = -1
    clust_list = []
    clust_ordered_name = input_file[0:-4] + '_ordered.txt' #filename for oredered cluster file
    
    if os.path.exists(clust_ordered_name):
        
        if overwrite == True:
            
            os.remove(clust_ordered_name)
            
        else:
            
            raise ValueError(clust_ordered_name + ' exists')
    
    order_fileobj = open(clust_ordered_name, 'a')
            
    clust_shortlist_name = input_file[0:-4] + '_best_matches.txt'
    
    if os.path.exists(clust_shortlist_name):
        
        if overwrite == True:
            
            os.remove(clust_shortlist_name)
            
        else:
            
            raise ValueError(clust_shortlist_name + ' exists')
            
    shortlist_fileobj = open(clust_shortlist_name, 'a')
    
    #in below loop, search for lowest clusters (i.e. 0 subclusters),
    #and when found, include all subsequent rows' filenames/RMSD distance to centroid
    #values, until we reach another @cluster line. At next cluster line, decide
    #if we reached a lowest cluster, and if yes, construct another entry in the
    #list of clusters using same procedure as before. If no, ignore all lines
    #until we hit another cluster line.
    for line in fileobj:
        
        if '@cluster' in line:
            
            row_list_clust = line.split()
            num_sub_clusters = int(row_list_clust[5]) #number of subclusters. if 0, we've hit a bottom cluster
            
            if num_sub_clusters == 0:
                
                cont = True
                clust_list.append([row_list_clust[1]]) #put cluster name at top of clust list
                clust_index = clust_index + 1 #increase index of clust_list
                
            else:
                
                cont = False
                
        elif cont == True:
            
            row_list_element = line.split() #split the line into a list
            clust_list[clust_index].append(row_list_element[1:3]) 
            #take elements [1] and [2], the filename and the rmsd distance to the centroid
            
    short_list = [] # to contain just the name of the clusters and the top members
    total_pop = 0
    
    #in the below loop, sort each cluster population by RMSD to centroid, 
    #count population, and append cluster and best match to centroid to the 'short_list'
    
    for i in range(0,len(clust_list),1):
        
        clust_list[i].sort(key = lambda x:x[1]) #this sorts based on 2nd element in row of cluster
                                                #evidently, the cluster name is kept at top of
                                                #clust_list[i]
                                                
        clust_pop = len(clust_list[i])-1       #cluster_population
        total_pop = total_pop + clust_pop                    
        clust_list[i][0] = [clust_list[i][0] ,(str(clust_pop))]
        #above line appends the population count of the given cluster to the cluster's
        #first row, such that the first row is [cluster_ID, population]
        
        short_list.append(clust_list[i][0:2]) #appends first two rows of clust_list
                                                #i.e. the cluster, its population
                                                #and the top match/RMSD distance to centroid
        
        for n in range(0,len(clust_list[i]),1): #write out the ordered cluster to the ordered cluster file
            new_line = ''
            
            new_line = new_line + clust_list[i][n][0] + '\t' + clust_list[i][n][1] + '\n'
            
            order_fileobj.write(new_line)
    
    order_fileobj.close()
    
    #Sort shortlist by cluster's fraction of population
    
    #First get fraction of total pop and append to shortlist for each element [i]
    for i in range(0,len(short_list),1):
        
        cluster_pop = short_list[i][0][1]
        fraction_pop = float(cluster_pop)/float(total_pop)
        
        short_list[i][0].append(fraction_pop)
        
    #then, sort shortlist by third element
    short_list.sort(key = lambda x:x[0][2], reverse = True)
    
    #write out the short list file
    
    first_line = 'clustID\tcount\tfrac_pop\tfile\tRMSD_to_centroid\n'
    shortlist_fileobj.write(first_line)
    
    for m in range(0,len(short_list),1):
        
        newLine = ''
        
        for k in range(0,len(short_list[m]),1):
            
            for n in range(0,len(short_list[m][k])):
            
                newLine = newLine + str(short_list[m][k][n]) + '\t'
        
        newLine = newLine + '\n'
        shortlist_fileobj.write(newLine)
        
    shortlist_fileobj.close()
        
    return(clust_list,short_list)


def retrieve_pdbs(match_file, #the file corresponding to shortlist_fileobj in above function,
                              #containing cluster IDs, counts, fraction of population,
                              #pdb file corresponding to structure closest to cluster centroid
                  pdb_frame_dir, #directory containing all the single frame pdbs.
                                  #you can use this to select the directory containing the pdbs
                                  #you did clustering on, or, if you did clustering on pdbs containing
                                  #structure interfaces, you can select a directory with the corresponding
                                  #full length structures.
                  output_dir, wdir = os.getcwd(), thresh = 0.01):
    
    #Overview, 
    
    #purpose: given match file containing filenames for closest to centroid
    #interface pdbs, and a directory where all the full size pdbs corresponding
    #to the interface pdbs are located, for each interface pdb in match file,
    #find corresponding full pdb (i.e. full pdb from which interface pdb was derived)
    #and copy the pdb to a directory for the subset of pdbs (output_dir)
    
    #added functionality to set a threshold for inclusion of pdbs into the 
    #subset folder. The threshold is a threshold for fraction of total population
    #which a cluster accounts for, and if the cluster passes this threshold,
    #the pdb corresponding to the closest structure to the cluster centroid is
    #copied into the subset directory
    
    default_dir = os.getcwd()
    os.chdir(wdir)
    
    if os.path.exists(output_dir):
        
        raise ValueError(output_dir + ' already exists')
        
    else:
        
        os.mkdir(output_dir)
    
    match_file_obj = open(match_file)
    N = 0
    for line in match_file_obj:
        
        N = N+1
        
        if N > 1: #we put this here to skip first line of match file
        
            row = line.split('\t')
            
            if float(row[2]) >= thresh:
                
                    pattern = 'md[0-9]*_[0-9]*'
                    match = re.search(pattern,line.split('/')[-1]) #find the pattern in the filename
                    pattern2 = match.group(0) #this retrieved pattern will be correspond to md simulation and frame number
                    #said pattern will be foudn in the filename of the corresponding full pdb file for the given simulation
                    # and frame, and that file only
                    pdb_list = os.listdir(pdb_frame_dir)
                    pdb_list.sort()
                    
                    
                    #conduct search for pdbfile
                    
                    cont = True
                    N = 0
                    first_el = 0
                    last_el = len(pdb_list) - 1
                    
                    while cont == True:
                    
                        if N > len(pdb_list):
                            
                            cont = False
                            print('could not find file corresponding to ' + pattern2)
                        
                        midpt_ind = (last_el - first_el)//2 + first_el
                        midpt = pdb_list[midpt_ind] #retrieve the filename as a string
                        midpt_match = re.search(pattern,midpt) # rfind a match to the pattern (ie. a pdb correspdoning to an md frame)
                        
                        if midpt_match: #if there's a match proceed with search iteration
                            
                            midpt_exp = midpt_match.group(0) #expression of same format as pattern2
                        
                            if pattern2 == midpt_exp:
                                
                                pdb_filepath = os.path.join(pdb_frame_dir, midpt)
                                
                                break
                            elif pattern2 < midpt_exp:
                                
                                last_el = midpt_ind -1
                                
                            elif pattern2 > midpt_exp:
                                
                                first_el = midpt_ind + 1
                                
                            else:
                                
                                raise ValueError('this condition should not be possible in the search')
                                
                        else: #otherwise, remove the midpoint file from the list as it cannot be what we're looking for
                            
                            pdb_list.remove(pdb_list[midpt_ind])
                            last_el = last_el -1
                            
                        N = N + 1
                    
                    #give a filename, containing cluster ID as well as ensemble/expt/md_set
                    # from which it was derived. By ensemble, I refer to the ensemble of
                    #md simulations that was run.
                    
                    clust_id_pattern = '^t.[0-9]*'
                    clust_id_match = re.search(clust_id_pattern,line)
                    clust_id = clust_id_match.group(0)
                    
                    old_filename = pdb_filepath.split('/')[-1]
                    ens_name_pattern = '[\w]*(?=_prod)' #reg expression to isolate ensemble name prefix
                    ens_name_match = re.search(ens_name_pattern,old_filename)
                    ens_name = ens_name_match.group(0)
                    
                    new_filename = ens_name + '_'+ clust_id + '.pdb'
                    newfile_path = os.path.join(output_dir, new_filename)
                    copy_command = 'cp ' + pdb_filepath + ' ' + newfile_path
                    subprocess.run(copy_command.split())
    
    match_file_obj.close()    
    os.chdir(default_dir)
                
            
            
        
#if __name__ == '__main__':

#    wdir = '/home/kimlab2/owenwhitley/amber_projects/5q0k_H++/071417_ensemble/interface_cluster'
#    
#    (a,b) = clust_parser(os.path.join(wdir,'cluster_output_md8_interface_0.75.txt'), overwrite = True)   
#    match_file = 'cluster_output_md8_interface_0.75_best_matches.txt'
#    pdb_frame_dir = 'PDBs'
#    output_dir = 'pdbs_nearest_to_centroid'
#    out_name_prefix = '5q0k_md8_cluster_'
#    
#    
#    retrieve_pdbs(match_file, pdb_frame_dir, output_dir, wdir, thresh = 0.2)           
            
        
    
    
            
            
            
    