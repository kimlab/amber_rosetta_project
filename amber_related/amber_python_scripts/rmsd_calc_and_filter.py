#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 11:18:44 2017

@author: owenwhitley
"""

import os
import sys
from matplotlib import pyplot as plt

sys.path.append(os.path.join(os.getcwd(),'../amber_python_utils'))

import pytraj_utils as pyu
import numpy as np
import argparse
import subprocess

parser = argparse.ArgumentParser(prog = "mdcrd filtering by rmsd, and printing plots",
                        description = '''

                  ''')

parser.add_argument('-wd','--working_directory', required = True, 
                    help = 'containing all input files, destination for all output but mdcrd copies')
parser.add_argument('-top', '--prmtop', required = True, 
                    help = 'prmtop file')
parser.add_argument('-o', '--output_dir', required = True)
parser.add_argument('-l', '--limit', required = True)
parser.add_argument('-m', '--mode', required = False)
parser.add_argument('-mass','--mass', required = False)
parser.add_argument('-msk', '--mask', required = False)

#general overview: search workin directory for the mem files,
#then for each mem file retrieve appropriate files, make a trajectory,
#calculate rmsd relative to start frame, make a plot of the rmsd,
#write rmsd to file, and put mdcrd without any above threshold rmsd at any timepoint into output folder

args = parser.parse_args()

wdir = args.working_directory
top = args.prmtop
output_dir = args.output_dir #directory for mdcrd files, made as a subdirectory
                            # of working directory
lim = float(args.limit) #threshold value for rmsd filter
mode = args.mode #mode for the filtering mechanism (less than or less than or equal)
mass = args.mass #do we mass weight RMSD?
mask = args.mask

#Set up default values for unspecified options, where applicable.
#Also convert mass to a boolean
if mode == None:
    mode = 'lt'
if mass == 'False':
    mass = False
elif ((mass == None) | (mass == True)):
    mass = True
else:
    raise ValueError('mass must be entered as True, False, or not entered at all')
    
if mask == None:
    mask = '!:WAT,CL-,Na+'
    
#check for existence of ouput directory
output_dir_path = os.path.join(wdir,output_dir)

if os.path.exists(output_dir_path):
    raise ValueError(output_dir_path + ' already exists. Remove directory or give different output directory name')
else:
    os.mkdir(output_dir_path)

dir_list = os.listdir(wdir)
os.chdir(wdir)

mem_file_list = []

for i in range(len(dir_list)):
    
    if 'mem_file_md' in dir_list[i]:
        mem_file_list.append(dir_list[i])
        
        
for n in range(len(mem_file_list)):
    
    #get mem_file
    mem_file = mem_file_list[n]
    #get sequence of mdcrd files
    filelist = pyu.obtain_mdcrd_seq(working_dir = wdir, mem_file = mem_file)
    #calculate rmsd for all frames across entire sequnce, relative to first frame
    rmsd_array = pyu.stitched_rmsd(working_dir = wdir, file_list = filelist, 
                                    top = top, mask = mask, mass = mass)
    #obtain a boolean conditioned on the presence of any above threshold
    #rmsd values
    keep_mdcrd = pyu.filter_rmsd(rmsd_array = rmsd_array, limit = lim, mode = mode)
    
    if keep_mdcrd:
        
        #if we're using the mdcrd file for further analysis, find its name, and copy it to the output directory
        mem_file_obj = open(mem_file, 'r')
        mem_filestr = mem_file_obj.read()
        prod_mdcrd = mem_filestr.split('\n')[2] #find name of production file
        cmd_string = 'cp ' + prod_mdcrd + ' ' + output_dir
        subprocess.run(cmd_string.split())
    
    #make a plot, give it a name corresponding to the md run, and save figure    
    rmsd_plot = plt.plot(rmsd_array)
    rmsd_plot_name = mem_file[0:-16] + 'rmsd' + mem_file[-8:-4] + '.png'
    plt.savefig(rmsd_plot_name)
    plt.clf()
    
    #save the info to a file
    rmsd_filename = rmsd_plot_name[0:-4] + '.txt'
    rmsd_file = open(rmsd_filename,'a')
    
    for m in range(len(rmsd_array) + 1):
        
        if m == 0:
            newLine = 'mass = ' + str(mass) + '\n mask = ' + str(mask) + '\n'
        else:
            newLine = str(m) + '\t' + str(rmsd_array[m-1]) + '\n'
        rmsd_file.write(newLine)
    
        
    