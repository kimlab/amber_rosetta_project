#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 09:24:34 2017

@author: owenwhitley
"""
import sys
import os
import argparse
import subprocess

parser = argparse.ArgumentParser(prog = "Amber MD input preparation",
                        description = '''
Generate properly formatted pdb for amber from rosetta generated pdb                  
                  ''')
parser.add_argument('-m','--mode', required = True, help = 'type denovo or rosetta\
under denovo mode, expected that you will be generating inputs for MD for \
a structure that has not been put through rosetta. For rosetta, expecting\
a folder with pdb files generated in rosetta, for which inputs for MD will be generated')
parser.add_argument('-ns', '--num_sim', required = True, help = 'num simulations')
parser.add_argument('-a', '--amber_rosetta_dir', required = True, help = 'directory to amber_rosetta python scripts')
parser.add_argument('-pps', '--proc_per_sim', required = True, help = '-processors per simulation')
parser.add_argument('-wt', '--wall_time', required = True, help = 'wall time for scinet job')
parser.add_argument('-st', '--script_template', required = False, help = 'template for bash script. should contain\
info for a typical job to run on cluster, including programs to load, processes per node. Number of nodes should be left blank, as well as wall time')
parser.add_argument('-nr', '--num_residues', required = False, help = 'number of residues \
on protein, needed if using default relax and equilibration protocols for amber')
parser.add_argument('-rlx', '--relax_mode', required = False, help = 'relax protocol\
(i.e. which amber input files to use for minimization protocol). Defaults to \'default\' protocol. See user guide for details')
parser.add_argument('-eq', '--equilibration_mode', required = False, help = 'heating\
/equilibration protocol for amber MD. Defaults to making an input file for \'default\' protocol.\
 See user guide for details.')
parser.add_argument('-prod', '--prod_mode', required = False, help = 'production\
 protocol for amber MD. Defaults to making an input file for default protocol.\
 See user guide for details')
parser.add_argument('-wd', '--wdir', required = False, help = 'working directory.\
expected that you have an H++ generated amber crd file and top file in this folder.\
will contain all input files relevant to MD in amber. Only use this option\
if you are using denovo mode')
parser.add_argument('-c', '--crd', required = False, help = 'amber coordinate file,\
must be provided if using denovo mode and must be located in working directory.')
parser.add_argument('-p', '--top', required = False, help = 'amber topology file, \
must be provided if using denovo mode and must be located in working directory.')
parser.add_argument('-nam', '--expt_name', required = False, help = 'experiment name\
, required if using denovo mode. Provides a prefix for all input files generated\
which will carry over to all outputs of MD and clustering. This is automatically\
generated in rosetta mode based on the directory given for output_dir option')
parser.add_argument('-out', '--output_dir', required = False, help = 'output directory\
for all MD related input files for rosetta generated pdbs, if mode is specified\
as rosetta. For every pdb file in pdb_folder, a subdirectory call output_dir_mut_n \
will be made, where n refers to the nth pdb. directly under the output directory\
will be a text file mapping the original rosetta pdbs to the corresponding subdirectory\
 containing their coordinates and other inputs ready for an amber MD simulation')

parser.add_argument('-sol', '--solvate', required = False, help = 'add solvation box with periodic\
 boundary conditions. Defaults to true, must type \'False\' to specify not.')

parser.add_argument('-ion', '--add_ions', required = False, help = 'add \
ions to neutralize the system. Enter ions as a string as you would in a tleap input file')

args = parser.parse_args()
mode = args.mode
ambrose_dir = args.amber_rosetta_dir
num_simulations = args.num_sim
proc_per_sim = args.proc_per_sim
wall_time = args.wall_time
script_template = args.script_template

if script_template == None:
    script_tempate = 'script_template.sh'
    
num_residues = args.num_residues

rlx_mode = args.rlx_mode
eq_mode = args.eq_mode
prod_mode = args.prod_mode
wdir = args.wdir

if r

ambdir = os.path.join(ambrose_dir,'amber_related')
amb_utils = os.path.join(ambdir,'amber_python_utils')
sys.path.append(amb_utils)


prep_md_inputs(    mode, #are we taking pdbs from rosetta or doing this de novo?
                   ambdir,
                   num_simulations,
                   proc_per_sim,
                   wall_time,
                   script_template = script_template,
                   num_residues = num_residues, #required to be of type int if using any of default protocols
                   rlx_mode = 'default',
                   eq_mode = 'default',
                   prod_mode = 'default',
                   wdir = None, #required if mode = denovo
                   crdfile = None, #required if mode = denovo, crdfile from H++ website
                   prmtop = None, #required if mode = denovo, topology file from H++ website
                   expt_name = None, #required if mode = denovo, variable will
                   #be specified automatically if mode = rosetta
                   output_dir = None, #required if mode = rosetta, 
                   #must be a string giving filepath to soon-to-exist folder 
                   #where subdirectories will contain all input files to run an md
                   #for a given rosetta generated pdb (one subfolder made for each)
                   #pdb in the pdb_folder from which we will be generating md inputs
                   pdb_folder = None, #required if mode = rosetta, 
                   #must be a string giving filepath to folder 
                   #containing pdbs genearted by rosetta
                   solvate = True, #if true, use explicit solvent with periodic boundaries
                   add_ions = None, #not strictly required, but needed if protein has a net charge
                   box_length = None, #requred if solvate = True
                   forcefield = 'leaprc.ff14SB')