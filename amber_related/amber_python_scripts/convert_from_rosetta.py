#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 14:58:37 2017

@author: owenwhitley
"""


import os
import argparse
import subprocess

parser = argparse.ArgumentParser(prog = "Rosetta -> Amber pdb conversion",
                        description = '''
Generate properly formatted pdb for amber from rosetta generated pdb                  
                  ''')
parser.add_argument('-pdb','--pdb_folder', required = True, help = 'folder containing all input files')
parser.add_argument('-out','--output_folder', required = True, help = 'folder containing all output subdirectories')
parser.add_argument('-ambdir','--amber_projects_dir', required = True, help = 'amber_projects directory. below this directory,\
                    need a \'data\' folder containing ffncaa frcmod files and ffncaa.in file, \
                    as well as amber_python_utils and amber_python_scripts folders containing appropriate python modules')
parser.add_argument('-sol','--solvate', required = True, help = 'solvate pdb? Do we use an explicit solvent?')
parser.add_argument('-len', '--solvate_box_length', required = False, help = 'solvate_box_length')
parser.add_argument('-io', '--addions', required = False, help = 'do we add ions?')
parser.add_argument('-ff', '--forcefield', required = False, help = 'forcefield used')
#parser.add_argument('-ovw' '--overwrite', required = False, help = 'overwrite, default = False')

args = parser.parse_args()

#assign all inputs to variables
pdbfolder = args.pdb_folder
output_folder = args.output_folder
ambdir = args.amber_projects_dir
solvate = args.solvate
box_length = args.solvate_box_length
add_ions = args.addions
print(add_ions)
forcefield = args.forcefield
print(forcefield)
#overwrite = args.overwrite

import sys
amber_py_utils = os.path.join(ambdir,'amber_python_utils')
sys.path.append(amber_py_utils)

import prep_from_rosetta as prep



if forcefield == None: #set default forcefield
    forcefield = 'leaprc.ff14SB'
    
if solvate == 'Y':
    solvate = True
elif solvate == 'N':
    solvate = False
else:
    raise ValueError('--solvate must be specified as Y for yes and N for no')
    
#if overwrite != None:
#    
#    if overwrite == 'Y':
#        
#        overwrite = True
#        
#    elif overwrite == 'N':
#        
#        overwrite = False
#        
#    else:
#        
#        raise ValueError('overwrite must be specified as Y for yes or N no, or left unspecified')
    
prep.prep_from_rosetta(pdbfolder, output_folder, ambdir, box_length,
                      solvate = True, add_ions = None,
                       forcefield = 'leaprc.ff14SB')
   