#!/bin/bash
#PBS -l nodes=2:ppn=12:gpus=4,walltime=1:00:00
cd $PBS_O_WORKDIR
module load intel/15.0.6
module load intelmpi/5.0.3.048
module load gcc/4.8.1
module load cuda/6.0
module load amber
