#!/bin/bash
#PBS -q gravity -l nodes=1:ppn=12:gpus=1,walltime=12:00:00
cd $PBS_O_WORKDIR
module purge
module load intel/15.0.6
module load intelmpi/5.0.3.048
module load gcc/4.8.1
module load cuda/6.0
module load amber/14.0
