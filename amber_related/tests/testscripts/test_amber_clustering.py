#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 17:00:52 2017

@author: owenwhitley
"""

import sys
sys.path.append('../../amber_python_utils')

import amber_clustering as clust
import os
import unittest


class test_amber_clustering(unittest.TestCase):
    
        
    
                
        
        
    def test_expected_outputs(self):
        
        input_pdb = '../ref_data/4b7w_6_chain_mockup.pdb'
        ref1 = '../ref_data/split_pdb_2chain_ref1.pdb'
        ref2 = '../ref_data/split_pdb_2chain_ref2.pdb'
        ref3 = '../ref_data/split_pdb_2chain_ref3.pdb'
        
    
        clust.split_pdb(input_file = input_pdb,
                    num_chains = 2,
                    output_name_root = '../output/split_2chain_test_')
    
        
            
        def count_line_matches(string1,string2):
            #idea: split strings by line, then the lines by whitespace
            #compare contents of each line, and count matches
            #if you don't have two strings with equal number of lines,
            #by default count = 0, and tests using this will fail
            lines_str1 = string1.split('\n') #list of line entries for string 1
            lines_str2 = string2.split('\n')
            
            if len(lines_str1) == len(lines_str2):
                count = 0
                
                for i in range(0,len(lines_str1),1):
                    strsplit_line1 = lines_str1[i].split()
                    strsplit_line2 = lines_str2[i].split()
                    
                    if strsplit_line1 == strsplit_line2:
                        count = count + 1
            else:
                count = 0
            
            return(count)
        
        def count_lines(string):
            return(len(string.split('\n')))
        
        self.ref1 = open(ref1)
        self.ref1str = self.ref1.read()
        self.ref2 = open(ref2)
        self.ref2str = self.ref2.read()
        self.ref3 = open(ref3)
        self.ref3str = self.ref3.read()
        self.test1 = open('../output/split_2chain_test_1.pdb')
        self.test1str = self.test1.read()
        self.test2 = open('../output/split_2chain_test_2.pdb')
        self.test2str = self.test2.read()
        self.test3 = open('../output/split_2chain_test_3.pdb')
        self.test3str = self.test3.read()
        
        self.assertEqual(count_lines(self.ref1str), count_line_matches(self.ref1str,self.test1str))
        self.assertEqual(count_lines(self.ref2str), count_line_matches(self.ref2str,self.test2str))
        self.assertEqual(count_lines(self.ref3str), count_line_matches(self.ref3str,self.test3str))
        
        os.remove('../output/split_2chain_test_1.pdb')
        os.remove('../output/split_2chain_test_2.pdb')
        os.remove('../output/split_2chain_test_3.pdb')
            
if __name__ == '__main__':
    unittest.main()