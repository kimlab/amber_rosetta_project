#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 16:33:40 2017

@author: owenwhitley
"""

#Overview:
#
# Purpose: generate mutant structures for a folder full of pdb files, sampling
# and getting rosetta score for every combination of specified residue positions
# and specified possible NCAA mutations

# Process: We have a group of pdb files and specify np processes. We divide
#           that group into np subgroups, with n files added to each subgroup,
#            n = #files/np rounded down. Any remaining files are added to the 
#           subgroups, one at a time. For example, if we had 3 remaining files to
#           assign to 4 groups, we would assign the first file to subgroup 1, the
#           second to subgroup 2, the third to subgroup 3. Once the subgroups are
#           defined, we start np processes that sequentially go through each
#           subgroup (1 subgroup assigned per process) and relax and generate
#           mutants for every pdb in the subgroup. 
#           
#               For a given pdb, the pdb
#           is cleaned (generateing a version with only ATOM and TER lines)
#           relaxed in rosetta (given the relax commands applied across all residues)
#           and then sequentially packed using resfiles with mutant residues
#           to generate all possible single mutants given a list of possible
#           NCAAs (possible mutations) and a list of possible residues (positions)
#
#Expected inputs: 
#=============== Required ==============
#   -pdb = path to folder containing pdb files. Mut be raw pdb files stripped of
#           repeat units (if existing in original pdb), or pdbfiles generated from
#           mdcrdfiles via amber_conversion.py script.Note that you 
#           CANNOT have any non-pdb files in this folder.
#   -in = path to folder containing input files (residue list, ncaa_list)
#   -out = path to output folder. Within this folder, will put each subdirectory containing output for
#       different structures
#   --rosetta_scripts_exe: full filepath to rosetta scripts executable
#   --rosetta_python_dir: path to folder containing kimlab developed rosetta python utilities and scripts
#   --residue_list: text file: list of residues, with each row formatted as\
#       residue  <whitespace> chain ID
#   -np: number of processors, really number of separate processes that this
#        script will start via the multiprocessing module
#============== Optional ===================
#   -- relax_commands: specified by 1 or more rc flags.commands given to rosetta when generating a relaxed\
#                   pdb structure. commands will be applied to all residues. 
#                   Commands written to resfile in the columns after residue 
#                   and chain specified. Note that each command must be specified
#                    individually by a -rc flag. If no commands specified, the default
#                   for this option is NATAA EX 1 EX 2. Example usage: -rc 'NATAA'
#                   Note that this can be entered on command line or as part of bash script
#                   as part of python command
#   -- pack_commands: same usage as relax_commands. Can be entered on command line
#                       or as part of bash script as part of python command. Default
#                       commands given to rosetta are NATAA EX 1 EX 2 for packing.
#   -- mut_commands: same as relax_commands, but only given to a residue being mutated.
#                   note that in the resfile used to generate mutant pdb, command will be
#                   <residue> <chain> EMPTY NC <ncaa mutated to> <mut_command 1> <mut_command_2> ...
#   -- ncaa_list: list of non-canonical amino acids you wish to use in mutations
#                   if left unspecified, the 
#Note that if these options are 

#Outputs:
#
#   for every pdb in pdb folder, several subdirectories:
#       -clean_pdb: contains cleaned pdb (All lines but ATOM and TER removed)
#       -raw_resfile: template resfile upon which editing is done to generate
#                       relax_resfile and pack_resfile. For more details about
#                       resfiles, consult Rosetta Commons. For more details on the 
#                       relax resfile and pack resfile, continue reading.
#       -mod_resfile: contains relax_resfile (resfile used in relax protocol)
#                       and pack_presfile (resfile template used to generate)
#                       resfiles for the pack protocol. For details on their use,
#                       see 'Algorithm'
#       -relaxed_pdb: contains pdb
#       -mutant_structures: Contains a subdirectory for every chain/residue position
#                           mutated. each subdirectory contains every possible
#                           mutant pdb at the given position (given list of NCAAs)
#                           and 
#
import sys
from multiprocessing import Process
import argparse
import os
import time

parser = argparse.ArgumentParser(prog = "KimLab Python-Rosetta Iterative Mutant Structure Generation",
                        description = '''
iteratively generate mutant structures via rosetta program.
                  
Using multiple CPUs, cut a list of pdbs by the number of processors into
np sub-lists, which, on separate CPUs, serially go through rosetta 
relax and mutant generation protocol
                  
                  ''')
parser.add_argument('-pdb','--pdb_folder', required = True, help = 'folder containing all input files')
parser.add_argument('-in','--input_directory', required = True, help = 'folder containing all input files')
parser.add_argument('-out','--output_directory', required = True,
                    help = 'destination for all output. subfolders wil be made to contain output of specific\
functions used in this python program')
parser.add_argument('--rosetta_scripts_exe', required = True,
                    help = 'executable for rosetta scripts')
parser.add_argument('--rosetta_python_dir', required = True, 
                    help = 'top directory for folders containing kimlab rosetta\
python utilities and scripts')
parser.add_argument('--relax_protocol', 
                    help = 'xml file with instructions for rosetta scripts\
if unspecified defaults to rosetta_fast_relax.xml')
parser.add_argument('--pack_protocol',
                    help = 'xml file with instructions for rosetta scripts\
if unspecified defaults to rosettaPack.xml')
parser.add_argument('--score_protocol',
                    help = 'xml file with instructions for rosetta scripts\
if unspecified defaults to rosettaScore_T2014.xml')
parser.add_argument('-rc','--relax_commands', action = 'append',
                    help = 'commands given to rosetta when generating a relaxed\
pdb structure. commands will be applied to all residues. Commands written to resfile\
  in the columns after residue and chain specified. Note that each command must\
  be specified individually by a -rc flag. If no commands specified, the default\
  for this option is NATAA EX 1 EX 2. Example usage: -rc \'NATAA\' \
  -rc \'EX 1\' gives commands NATAA EX 1.')
parser.add_argument('-pc','--pack_commands', action = 'append', help =
                    'commands applied to all residues (except mutated residue)\
 when going through a packing protocol in rosetta. Commands written to resfile\
  in the columns after residue and chain specified. Note that each command must\
  be specified individually by a -pc flag. If no commands specified, the default\
  for this option is NATAA EX 1 EX 2. Example usage: -pc \'NATAA\' \
  -pc \'EX 1\' gives commands NATAA EX 1.')
parser.add_argument('-mc', '--mut_res_commands', action = 'append', help = 
 'commands given to rosetta when designing and packing a mutant structure. \
 if left unspecified, defaults to EX 1 EX 2. The residue to be mutated is \
 altered in the resfile to have the commands EMPTY NC NCAA_ID (one of the ncaas\
 from your list of ncaas) and any commands from\
 mc will be appended to those commands. Note that these commands only apply to \
 the mutant residue. Also note that commands must be specified individually \
 with individual mc commands. E.g., to append EX 1 EX 2 to the mutant commands\
  you would type -mc \'EX 1\' -mc \'EX 2\'')
parser.add_argument('--ncaa_list', help = 
'list of NCAAs that will be substituted at specified residue positions.\
 if unspecified, will default to NCAA_list.txt in $rosetta_python_folder/NCAA_list.\
 Note that all NCAAs MUST have existing rotlib files in \
  $PATH_TO_ROSETTA/main/database/rotamer/ncaa_rotlibs/ncaa_rotamer_libraries/alpha_amino_acid')
parser.add_argument('--residue_list', required = True, help = 
                    'list of residues, with each row formatted as\
residue # <whitespace> chain ID.')



parser.add_argument('-np', '--num_processors', required = True)
args = parser.parse_args()

input_dir = args.input_directory
output_dir = args.output_directory
rosetta_scripts_exe = args.rosetta_scripts_exe
    
#Add rosetta_python_utils folder to the path and import some of the utils
rosetta_python_path = args.rosetta_python_dir
rosetta_py_utils_path = os.path.join(rosetta_python_path, 'rosetta_python_utils')
sys.path.append(rosetta_py_utils_path)

import rosetta_interfaces as RI 

xml_folder = os.path.join(rosetta_python_path, 'rosetta_scripts_xmls')

relax_protocol = args.relax_protocol

#set default settings if options not specified
if relax_protocol == None:
    relax_protocol = os.path.join(xml_folder, 'rosetta_fast_relax.xml')
else:
    relax_protocol = os.path.join(input_dir,relax_protocol)

pack_protocol = args.pack_protocol

if pack_protocol == None:
    pack_protocol = os.path.join(xml_folder, 'rosettaPack.xml')
else:
    pack_protocol = os.path.join(input_dir, pack_protocol)
    
score_protocol = args.score_protocol
if score_protocol == None:
    score_protocol = os.path.join(xml_folder, 'rosettaScore_T2014.xml')
else:
    score_protocol = os.path.join(input_dir, score_protocol)

ncaa_list_file = args.ncaa_list
ncaa_list = os.path.join(input_dir,ncaa_list_file)

if ncaa_list_file == None:
    ncaa_list_folder = os.path.join(rosetta_python_path, 'NCAA_list')
    ncaa_list = os.path.join(ncaa_list_folder, 'NCAA_list.txt') #default NCAA list if none specified

pdb_folder = args.pdb_folder

residue_list = args.residue_list

residue_list_filepath = os.path.join(input_dir,residue_list)

relax_commands = args.relax_commands

if relax_commands == None:
    relax_commands = ['NATAA','EX 1', 'EX 2']

pack_commands = args.pack_commands

if pack_commands == None:
    pack_commands = ['NATAA','EX 1', 'EX 2']
    
mut_res_commands = args.mut_res_commands

if mut_res_commands == None:
    mut_res_commands = ['EX 1', 'EX 2']
    
rosetta_scripts_exe = args.rosetta_scripts_exe
np = int(args.num_processors)


#Now to the task of dividing the group of pdbs into equally or near equally sized groups for np processes.
#Basically, we divide M pdbs by np processes to get int(M/np) sized lists.
#If M/np is not an integer, we first put int(M/np) filenames in each list,
#and with the remaining filenames allocate them one per list until no pdb filenames remain
#With the pdbfiles organized into subgroups, we can sequentially go through each subgroup
#to generate mutants for each pdb in a given subgroup, and do this in a separate
#process for each subgroup.

pdb_list = os.listdir(pdb_folder)
print(pdb_list)
list_len = len(pdb_list)

quot1 = int(list_len/np)
pdb_array = [] #to contain several lists within one list. each sub list is assigned to a job during multiprocessing
N = 0

for i in range(0,np,1):
    pdb_array.append(pdb_list[N:N+quot1]) #add 1st quot1 items to the list
    N = N+quot1
rem = (list_len % np)

if rem != 0: #allocate remaining items to the pdb lists if necessary.
    
    for n in range(0,rem,1):
        pdb_array[n].append(pdb_list[N])
        N = N + 1

start = time.time()

print(pdb_array[:])

for m in range(0,np,1): #spawn the processes
        
    proc_list = pdb_array[m]
    p = Process(target = RI.mut_pack_pdb_seq, 
                args = ( proc_list, #list of pdbs to access
                         pdb_folder, #Will still draw pdb files from the pdb folder
                         rosetta_scripts_exe, #Filepath of the rosetta scripts executable
                         relax_protocol, #XML you'd like to use for relax protocol
                         pack_protocol, #XML you'd like to use for the mutate and pack protocol
                         score_protocol,
                         output_dir, #parent dir for all output directories
                         ncaa_list,
                         residue_list_filepath, #list of chain/residue position to mutate
                         relax_commands,
                         pack_commands,
                         mut_res_commands,
                         ))
    p.start()
    if m == np-1:
        p.join() #hold off on calling the end until last process started terminates.


end = time.time()
print(end-start)    
    