#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 16:39:23 2017

@author: owenwhitley
"""

#Testing of new score table thing

import sys
sys.path.append('/home/kimlab2/owenwhitley/rosetta_project/rosetta_python_utils')
rosetta_ensemble_dir = '/home/kimlab2/owenwhitley/rosetta_project/5q0k_071417_ensemble'
ensemble_name = '5q0k_prod'

import analysis_utils as AU

AU.make_scoretable_ensemble(rosetta_ensemble_dir, ensemble_name, overwrite = True,
                            cutoff = False,
                            rlx_diff_cutoff = 0,
                             cln_diff_cutoff = 0)
