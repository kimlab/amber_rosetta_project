#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 10:40:26 2017

@author: owenwhitley
"""

import sys
import os


import argparse

parser = argparse.ArgumentParser(prog = "conversion of pdbs from clustering procedure",
                        description = '''

                  ''')

parser.add_argument('-in','--input_directory', required = True, 
                    help = 'folder containing all input pdn files')
parser.add_argument('-out', '--output_directory', required = True, 
                    help = 'folder location for output pdb files')
parser.add_argument('-ref', '--chain_reference', required = True, 
                    help = 'filepath to chain ref file. each line should\
 have format \'chainID start_res_num end_res_num\' ')
parser.add_argument('--amber_rosetta_project_path', required = True)

#note: input directory can be specified as a relative path, as can chain reference, output directory
# and topology

#Overview: After you perform clustering on md trajectory derived PDBs,
# you obtain a group of pdb files, each one the closest structure to the
# cluster centroid for a given cluster. After this, the pdb files are still formatted
# as they were when outputed by pytraj, so it is necessary to reformat pdbs
# and rename residues (where applicable) so that they can be input to the rosetta
# mutation protocol. After running a list of pdbs through this script, you
# will have the specified output directory filled with the near-centroid structures,
# that are ready for the mutation protocol.

#expected inputs: folder of pdb files from your clustering procedure,
# chain_ref text file (text file with each line as 'chain\tstart\tfinish' to indicate 
# start and end numbers for each chain)

#output: in output folder, a pdb corresponding to each pdb of input folder, 
#numbering edited to reflect original numbering of pdb file (provided chain file)
#had chains and their starts and ends labelled correctly


args = parser.parse_args()
input_dir = args.input_directory
output_dir = str(args.output_directory)
chainfile = args.chain_reference
amber_rosetta_path = args.amber_rosetta_project_path
python_dir = os.path.join(amber_rosetta_path,'rosetta_related')
print(python_dir)
data_folder = os.path.join(python_dir,'data') # contains mapping file
aa_mapping = os.path.join(data_folder,'rosetta_amb_code_map_short.csv')
chain_ref_dir = os.path.join(data_folder,'chain_references')
#above variable is path to file that maps ncaas from amber to rosetta

python_utils_dir = os.path.join(python_dir, 'rosetta_python_utils')

sys.path.append(python_utils_dir)
print(python_utils_dir)

import amber_conversion_utils as ambcon

input_pdb_list = os.listdir(input_dir)

print(input_pdb_list)    
for i in range(0,len(input_pdb_list),1): #for each pdb in the folder
    
    print(i)
    pdb_filename = input_pdb_list[i]
    pdb_filepath = os.path.join(input_dir,pdb_filename) #make filepath to pdb file    
    print(pdb_filename)
    output_rcn_name = input_pdb_list[i][0:-4] + '_rcn.pdb'
    out_pdb_rcn_path = os.path.join(output_dir,output_rcn_name)
    ambcon.redo_chain_num(input_pdb = pdb_filepath, output_pdb = out_pdb_rcn_path, 
                   chainfile = chainfile, overwrite = True)
    
    in_pdb_his_path = out_pdb_rcn_path
    out_pdb_his_name = pdb_filename[0:-4] + '_renam_his.pdb'
    out_pdb_his_path = os.path.join(output_dir, out_pdb_his_name)
    
    ambcon.pdb_rename_HIS(in_pdb_file = in_pdb_his_path, 
                   out_pdb_file = out_pdb_his_path,
                   overwrite = True,
                   verbose = True)
    
    output_filename = pdb_filename[0:-4] + '_prepped.pdb'
    ambcon.aa_rename_amber_to_rosetta(out_pdb_his_path, 
                                  output_filename, 
                                  output_dir, 
                                  aa_mapping, 
                                  overwrite = True)
    
    os.remove(out_pdb_rcn_path)
    os.remove(out_pdb_his_path)