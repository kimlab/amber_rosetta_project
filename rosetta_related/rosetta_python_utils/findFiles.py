# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os

def find_file(fName, #filename 
              path, #root directory below which you want to do a recursive search
              returnFirst = True):
    fileList = []
    for root, dir, files in os.walk(path):
        if fName in files:
            if returnFirst == True:
                return(os.path.join(root,fName))
                break
            else:
                fileList.append(os.path.join(root,fName))
    
    if returnFirst == False:
        return(fileList)
    
def show_files_folders(path): #recursively serach under the directory 'path' and list directories and files
    
    for root, dir, files in os.walk(path):
            print(root)
            print(dir)
            print(files)
            
            
def find_directory(dirName, #directory name (not full path)
                   path, #root path under which you want to search for the directory+
                   returnFirst = True):
    dirList = []
    for root, dir, files in os.walk(path):
        if dirName in dir:
            if returnFirst == True:
                return(os.path.join(root,dirName))
                break
            else:
                dirList.append(os.path.join(root,dirName))
    if returnFirst == False:
        return(dirList)
    
def return_filelist(path): #return a list of all files underneath a given path
    fileList = []
    for root, dir, files in os.walk(path):
        fileList.append(files)
    return(fileList)
