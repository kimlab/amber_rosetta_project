#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 09:29:41 2017

@author: owenwhitley
"""

#import pytraj as pytr
import os

#below function no longer needed
#def mdcrd_to_pdb(mdcrd_file,
#                 top, #topology
#                 output_dir, 
#                 frame_index):
#    
#    #conversion of mdcrd file to pdb file. No longer needed since clustering procedure results in
#    #production of pdb files
#    
#    traj = pytr.load(mdcrd_file,top = top, mask = '!:WAT,CL-,Na+', frame_indices = [frame_index])
#    
#    
#    mdcrd_filename = mdcrd_file.split('/')[-1]
#    pdb_filename = mdcrd_filename[0:-6] + '.pdb'
#    pdb_filepath = os.path.join(output_dir,pdb_filename)
#    pytr.write_traj(pdb_filepath,traj, overwrite = True)
#   


#============================================================================== 
def redo_chain_num(input_pdb, #filepath to input pdb
                   output_pdb, #filepath to pdb file to be written as output
                   chainfile, #see details under purpose
                   overwrite = False):
    
    #Purpose: amber automatically renumbers residue numbers to go from
    # 1 (first residue in pdb file) to #residues (last residue).
    # given a reference file (chainfile), redo the chain identification
    # for each residue and redo the numbering, and output the result as a pdb file.
    # The input pdb should be coming from one of the selected  pdbs
    # from the clustering procedure. Chainfile should be a text file
    # with each row of format <chain>\t<start_residue_number>\t<end_residue_number>
    
    chainfile_obj = open(chainfile,'r')
    chain_list = []
    
    if (os.path.exists(output_pdb)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(output_pdb):
        os.remove(output_pdb)
    
    for line in chainfile_obj:
        chain_list.append(line.split())
    
    inp_fileobj = open(input_pdb, 'r')
    out_fileobj = open(output_pdb, 'a')
    
    N = 0 #Index added to start as we give residue numbers
    chain_ind = 0 #Index for chain info in chain_list
    #Setup chain ID, chain start and end
    
    chain_start = int(chain_list[chain_ind][1]) #start of given chain
    chain_end = int(chain_list[chain_ind][2]) #end of gven chain
    chain_ID = str(chain_list[chain_ind][0]) #chain ID
    first_time = True
    
    for line in inp_fileobj:
        
        if line[0:4] == 'ATOM':
            
            
            
            if first_time == True:
                first_time = False
                lastTer = False
                
            else:
                
                if (last_inp_resnum < int(line[22:26])) & (lastTer == False):
                    #If the last residue number was less than the one currently being indexed,
                    #and the last line 
                    N = N + 1
                elif lastTer == True:
                    lastTer = False
            res_num = chain_start + N #'true' residue number, i.e. the number we are resetting the residue number to 
            res_num_string = '    ' #length of string matters here, should be 4 characters long, space.
            res_num_string = res_num_string[0:(len(res_num_string)-len(str(res_num)))] + str(res_num)
            newLine = line[0:21] + chain_ID + res_num_string + line[26:len(line)]
            last_inp_resnum = int(line[22:26]) #
            
            serial = int(line[6:11]) #serial number of residue
            res_name = line[17:21]    #residue name
                    
                
            if N > (chain_end-chain_start + 1):
                raise ValueError('At residue ' + str(res_num) + ' chain ' + chain_ID +
                                 ' residue number exceeds stated end residue number ' + 
                                 str(chain_end))
            
        elif line[0:3] == 'TER':
            
            if  N != (chain_end-chain_start):
                raise ValueError('TER line should only come after the stated' +
                                 ' end of the chain, for chain ' + chain_ID)
            N = 0
#            newLine = line[0:22] + res_num_string + line[len(line)-1]
            #Constructing a TER line
            record_name = 'TER   '
            serial = serial + 1
            empty = ' ' #empty string
            serial_str = empty*5
            serial_str = serial_str[0:(len(serial_str)-len(str(serial)))] + str(serial)
            filler = empty*6 #fill columns 12:17
            #res_name already specified
            #chainID already specified
            #residue sequence specifier (res_num_string) already specified
            
            newLine = (record_name + serial_str + filler + res_name + chain_ID + res_num_string + ' \n')
            
            if chain_ind != (len(chain_list)-1):
                chain_ind = chain_ind + 1
                chain_start = int(chain_list[chain_ind][1])
                chain_end = int(chain_list[chain_ind][2])
                chain_ID = str(chain_list[chain_ind][0])
                #Change chain index and chain related variables for new chain
                lastTer = True #If this is true, for the next atom line, N will not be incremented
                #lastTer is reverted to false after going through the next atom line
                
                serial = int(line[6:11])
                res_name = line[17:21]
                
        elif 'ENDMDL' in line:
            #Constructing a TER line
            record_name = 'TER   '
            serial = serial + 1
            empty = ' ' #empty string
            serial_str = empty*5
            serial_str = serial_str[0:(len(serial_str)-len(str(serial)))] + str(serial)
            filler = empty*6 #fill columns 12:17
            #res_name already specified
            #chainID already specified
            #residue sequence specifier (res_num_string) already specified
            
            newLine = (record_name + serial_str + filler + res_name + chain_ID + res_num_string + ' \n')
            
            
        else:
            
            newLine = line
            
        out_fileobj.write(newLine)
#==============================================================================
        
def change_temp_factor(in_pdb_file, #filepath to input pdb file
                       out_pdb_file, #filepath to pdb file to be written as output
                       overwrite = False):
    
    #This function is unncessary as I've found that rosetta is able to run without
    #changing the temperature factor section of the pdb file.
    
    #Basically, for every ATOM line, change columns 61:66 from 0.00 to 0.50
    
    if (os.path.exists(out_pdb_file)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(out_pdb_file):
        os.remove(out_pdb_file)
    
    inp_file_obj = open(in_pdb_file, 'r')        
    output_file_obj = open(out_pdb_file, 'a')
    
    for line in inp_file_obj:
        
        if line.split()[0] == 'ATOM':
            
            if line[62:66] == '0.00':
                
                newLine = line[0:62] + '0.50' + line[66:len(line)]
            
            else:
                
                newLine = line
                
        else:
            newLine = line
            
        output_file_obj.write(newLine)

#==============================================================================
        
def pdb_rename_HIS(in_pdb_file, #filepath to pdb file
                   out_pdb_file, #filepath to outputfile to be created
                   overwrite = False, verbose = True):
    
    #Rename histidines.
    #If we come across a line with an amber named histidine, rename the residue
    #name to HIS
    
    if (os.path.exists(out_pdb_file)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        print(os.path.exists(out_pdb_file))
        print(overwrite)
        
    elif os.path.exists(out_pdb_file):
        os.remove(out_pdb_file)
    
    inp_file_obj = open(in_pdb_file, 'r')    
    targets = ['HIP','HID','HIE']
    
    output_file = open(out_pdb_file, 'a')
    edited_residues = []
    
    for line in inp_file_obj:
        
        cont = False
        
        for i in range(0, len(targets), 1):
            
            if targets[i] in line:
                cont = True
                
        if cont == True:
            newLine = line[0:17] + 'HIS' + line[20:len(line)]
            resnum = line[23:29]
            res_chain = str(resnum) + line[21]
            
            if (res_chain in edited_residues) == False:
                edited_residues.append(res_chain)
        else:
            newLine = line
            
        output_file.write(newLine)
    
    if verbose == True:    
        if len(edited_residues) > 0:
            
            print('The following residues were recognized as histidine and\
     edited to HIS:')
            
            print(edited_residues[:])
            
        else:
            print('No residues recognized as being histidine with amber naming')
            
import parseText as pt

#==============================================================================

def aa_rename_amber_to_rosetta(pdb_filepath, #filepath to pdb input file
                               output_filename, #name (not path) of output file
                               output_folder, #directory containing output file
                               aa_mapping,overwrite = False):

    
#Purpose of function: rename NCAAs in pdbs with rosettaa named NCAAs     

#expected inputs: pdb filepath, with a pdb file that was outputted from rosetta
#               aa mapping as a filepath, with a csv file that contains the 
#               mapping orf rosetta names ot amber names

#output: pdb file with changed names for NCAAs

    
    pdbFileObj = open(pdb_filepath,'r')
    aa_map_fileObj = open(aa_mapping,'r')
    
    if output_filename == None:
        pdb_filename = pdb_filepath.split('/')[-1]
        output_filename = pdb_filename[0:-4] + '_ncaa_renam.pdb'
        
    output_filepath = os.path.join(output_folder,output_filename)
    
    
    #Are we overwriting? If not, and file with same name as output file exists,
    #don't make output file. If yes, remove the existing file with same name
    if (os.path.exists(output_filepath)) & (overwrite == False):
        raise ValueError('file exists with same name' + output_filepath + 
                         ' as output file, overwirte specified as False')
        
    elif os.path.exists(output_filepath):
        os.remove(output_filepath)
    
    output_file_obj = open(output_filepath, 'a')
    
    aa_map_array = pt.parseText_RE(aa_map_fileObj,'[,\n]')
    
    rosetta_names = []
    amber_names = []
    seen_names = []
    
    #create list of rosetta NCAA names and corresponding list of amber NCAA names
    for i in range(1,len(aa_map_array),1):
        
        rosetta_names.append(aa_map_array[i][0])
        amber_names.append(aa_map_array[i][2])
     
    for line in pdbFileObj:
        
        #Iteratively go through pdb file line by line, editing
        #any lines with NCAA residue on ATOM or TER line
        
        if (line[0:4] == 'ATOM') | (line[0:3] == 'TER'):
            
            #If ATOM or TER line, see if residue name is in list of rosetta_names
            #If yes, find the index of the rosetta name, and find approrpiate
            #amber name. Reconstruct the line as a newLine, with new residue incorporated

            res_name = line[17:20]
            
            if res_name in amber_names:
                
                for n in range(0,len(amber_names)):
                    
                    if res_name == amber_names[n]:
                        #if you find a rosetta name matching the residue name, save index
                        rosetta_index = n
                        
                        break
                new_res_name = rosetta_names[rosetta_index]
                newLine = line[0:17] + new_res_name + line[20:len(line)]
                
                if (new_res_name in seen_names) == False:
                    
                    seen_names.append(new_res_name)
            else:
                newLine = line
                
        else:
            #If not an ATOM or TER line, just write the line to the file
            newLine = line
            
        output_file_obj.write(newLine)
    
    outputs = [output_filename,seen_names]
    return(outputs)