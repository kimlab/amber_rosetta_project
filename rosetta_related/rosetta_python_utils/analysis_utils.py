#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 15:30:35 2017

@author: owenwhitley
"""
import os
import numpy as np
import parseText as pt
import re
#import pytraj as pytr


#Calculating buried surface area will have to wait for now
#def calc_buried_sa(pdb_file,chain_ref,exclusion_masks,solv_radius):
        

        

def make_scoretable_ensemble(rosetta_ensemble_dir, #directory under which all your rosetta_outputs are contained.
                                                 #The directory and its outputs should have been made via
                                                 #mut_pdb_seq or multiprocess_rosetta_mut from rosetta_interfaces module.
                                                 #See note on directory architecture
                             ensemble_name, #really just a string by which to identify the subfolder for each group
                                             #string must be present in each one of the subfolders and not in any other subfolders
                                             #that may exist.
                             overwrite = False,
                             cutoff = False, #do cutoffs for difference in rosetta score relative to relaxed pdb and cleaned pdb
                             rlx_diff_cutoff = 0.0, #mutants with differences in rosetta score relative to relaxed pdb > this value 
                                                     # are not included if cutoff == True
                             cln_diff_cutoff = 0.0,#mutants with differences in rosetta score relative to cleaned (unrelaxed) pdb > this value 
                                                     # are not included if cutoff == True
                             rank_by = 'group' #Do we order rows by group or by rosetta score?
                                             #If ranking by group (alphabetical order), break ties with rosetta score 
                                             #(lower rosetta scores before higher ones).
                                             #If ranking by rosetta score, order rows first by rosetta score
                                             #with lower scores taking higher precedence, then breaking ties by group
                             ):
    
    #Overview: For all mutants generated in rosetta mutation protocol,
    #generate a scoretable with listing the following info for each mutant:
    # 1) group: all mutants derived from the same input pdb structure from an MD ensemble are 
    #said to be from the same group. 
    # 2) chain: mutated chain 
    # 3) residue number: residue position on chain
    # 4) substitution : name of NCAA substituted in mutation
    # 5) total score: total rosetta score of mutant
    # 6) total score of relaxed pdb (total rosetta score)
    # 7) total score - total score of relaxed pdb
    # 8) total score of cleaned (unrelaxed) pdb
    # 9) total score - total score of cleaned pdb
    
    #Note on expected architecture of rosetta_ensemble_directory
    #   recall that for each input pdb for the rosetta mutation protocol,
    #   we get a subdirectory (with the name of the pdb + '_output'),
    #   which will contain the cleaned pdb, the relaxed pdb, template resfiles,
    #   and a subdirectory for mutant structures. Within the subdirectory for mutant
    #   structures, which will be called mutant_structures, there will be 
    #   a subdirectory for every chain and position combination for mutation
    #   protocol. Within each of those subdirectories will be all of the 
    #   mutant structures for the given position, as well as the corresponding
    #   rosetta scorefiles
    
    # General workfolow:
    #   for each group directory, if its name contains the ensemble name we give this function
    #   go in and find the folders for the relaxed pdb, the cleaned pdb, and the
    #   mutant structures folder. for the relaxed and cleaned pdb folders, retrieve
    #   the relaxed pdb and cleaned pdb rosetta scores from the appropriate scorefiles.
    #   for the mutant structures directory, go into each folder, and obtain
    #   chain, residue, mutation, and rosetta score for each mutant structure.
    #
    #   After going through all of these folders, you should have the 
    #   relaxed pdb score and the cleaned pdb score, as well as a list
    #   variable with all of the group, chain, residue num, mutation, 
    #   and rosetta score information you just collected. With this
    #   calculate the change in rosetta score for each mutant relatuive to the
    #   clean and relaxed pdb structures. and add the info to the list variable
    #   Once you have this done, write the info in the list to a file
    #   
    
        data_array = [[],[],[],[],[],[],[],[],[]]
        #2D array with the following
                        #Column 1: group
                        #Column 2: chain
                        #Column 3: residue num
                        #Column 4: substitution (NCAA substitution)
                        #Column 5: total score (total rosetta score for mutant)
                        #Column 6: total score of relaxed pdb (total rosetta score)
                        #Column 7: total score - total score of relaxed pdb
                        #Column 8: total score of cleaned pdb
                        #Column 9: total score - total score of cleaned pdb
                        
        #column ordering preserved for output text file
        
        output_dir = rosetta_ensemble_dir                
        found_a_group = False
        with os.scandir(output_dir) as it1:
            
            for entry in it1:
                
                if entry.is_dir() & (ensemble_name in entry.name):
                    
                    found_a_group = True
                    group_name = entry.name
                    group_path = os.path.join(output_dir,group_name)
                    
                    with os.scandir(group_path) as it2:
                        
                        #when I refer to group, I presume the group will be one
                        #of several sets of mutant pdbs derived from a single
                        #pdb that was derived from an md simulation
                        #once you're in a group directory \, find relaxed file
                        #find its score, and store that score
                        
                        #find 
                        found_relaxed = False
                        
                        counter = 0 
                        #This corresponds to 1 group directory, counting number of times a scorefile
                        #is picked up. Purpose is to add appropriate number of 
                        #relaxed pdb rosetta score copies to the data_array
                        #The columns of mutant structure score and relaxed pdb score
                        #will be converted to numpy arrays, and then the difference
                        #in score calculated (as a numpy array), will be calculated
                        
                        for entry in it2:
                            #within group directory, find relaxed folder and mutant structures directory
                            
                            if entry.is_dir() & (entry.name == 'relaxed_pdb'):
                                
                                relaxed_folder = entry.name
                                relaxed_dir = os.path.join(group_path, relaxed_folder)
                                rlx_dir_list = os.listdir(relaxed_dir)
                                
                                for i in range(0,len(rlx_dir_list),1):
                                    
                                    if '.sc' in rlx_dir_list[i]:
                                        
                                        #once in relaxed directory, if you find the scorefile, retrieve score
                                        
                                        rlx_scorefile = rlx_dir_list[i]
                                        rlx_fileObj = open(os.path.join(relaxed_dir, rlx_scorefile), 'r')
                                        rlx_parsed = pt.parseText2(rlx_fileObj)
                                        rlx_score = float(rlx_parsed[2][1])
                                        
                                        found_relaxed = True 
                                        
                                if found_relaxed == False:
                                    
                                    print('no relaxed pdb scorefile found in ' + group_path)
                                    break
                            
                            elif entry.is_dir() & (entry.name == 'mutant_structures'):
                                
                                #move into mutant_structures folder
                                
                                mut_str_folder = entry.name #mut structure parent folder
                                mut_dir_path = os.path.join(group_path, mut_str_folder)
                                
                                with os.scandir(mut_dir_path) as it3:
                                    
                                    for entry in it3:
                                        
                                        if entry.is_dir():
                                            
                                            chain_res_folder = entry.name
                                            chain_res_dir_path = os.path.join(mut_dir_path,chain_res_folder)
                                            chain_res_files = os.listdir(chain_res_dir_path)
                                            
                                            for n in range(0,len(chain_res_files),1):
                                                #we are now in the folder containing mutant pdbs and their scores
                                                if '.sc' in chain_res_files[n]:
                                                    
                                                    scorefile_name = chain_res_files[n]
                                                    scorefile_path = os.path.join(chain_res_dir_path, scorefile_name)
                                                    scorefile_obj = open(scorefile_path, 'r')
                                                    scorefile_parsed = pt.parseText2(scorefile_obj)
                                                    mut_score = scorefile_parsed[2][1] #total rosetta score for individual pdb
                                                    
                                                    #retrieve chain, residue and substitution info
                                                    scorefile_name_split = scorefile_name.split('_')
                                                    chain = scorefile_name_split[4]
                                                    residue = scorefile_name_split[2]
                                                    sub = scorefile_name_split[-1][0:-3] #.sc will be attached to end, need to remove
                                                    
                                                    #Add chain, residue, substitution, and score to one 'row' of data_array
                                                    data_array[0].append(group_name)
                                                    data_array[1].append(chain)
                                                    data_array[2].append(residue)
                                                    data_array[3].append(sub)
                                                    data_array[4].append(float(mut_score))
                                                    counter = counter + 1
                            elif entry.is_dir() & (entry.name == 'cleaned_pdbs'): #if we've foudn directory for clean pdbs
                               
                                cleaned_folder = entry.name
                                cleaned_dir = os.path.join(group_path,cleaned_folder)
                                cleaned_dir_list = os.listdir(cleaned_dir)                                
                                
                                for i in range(0,len(cleaned_dir_list),1):
                                    
                                    if '.sc' in cleaned_dir_list[i]:
                                        
                                        #once in relaxed directory, if you find the scorefile, retrieve score
                                        
                                        cleaned_scorefile = cleaned_dir_list[i]
                                        cleaned_fileobj = open(os.path.join(cleaned_dir, cleaned_scorefile), 'r')
                                        cln_scr_parsed = pt.parseText2(cleaned_fileobj)
                                        cln_score = float(cln_scr_parsed[2][1])
                                        
                                        found_relaxed = True 
                            
                        data_array[5] = data_array[5] + [rlx_score]*counter
                        data_array[7] = data_array[7] + [cln_score]*counter
        
        #search of directories has ended. Now calculate differences in rosetta score
        # relative to relaxed and cleaned structures
        if found_a_group == False:
            raise ValueError('did not find any folders under output_dir ' + output_dir +
                             ' with ' + ensemble_name + ' in the folder name. Check that ensemble_name was entered correctly')
        
        mutant_score_vect = np.array(data_array[4])
        relaxed_score_vect = np.array(data_array[5])
        cleaned_score_vect = np.array(data_array[7])
        neg_rlx_score_vect = np.multiply(-1,relaxed_score_vect)
        neg_cln_score_vect = np.multiply(-1,cleaned_score_vect)
        rlx_score_diff = np.add(mutant_score_vect,neg_rlx_score_vect)
        cln_score_diff = np.add(mutant_score_vect,neg_cln_score_vect)
        
        #and assign to 7th column (for mutant - relaxed) and 9th column of data array
        data_array[6][:] = rlx_score_diff[:]  
        data_array[8][:] = cln_score_diff[:]
        
        #make a file and write all the data to it
        
        score_table_name = ensemble_name + '_ensemble_scoretable.txt'
        
        score_table_path = os.path.join(output_dir,score_table_name)
        
        if (os.path.exists(score_table_path)) & (overwrite != True):
            raise ValueError(score_table_path + ' exists and overwrite != True')
        elif (overwrite == True) & (os.path.exists(score_table_path)):
            os.remove(score_table_path)
        
        
        len0 = len(data_array[0])
        len1 = len(data_array[1])
        len2 = len(data_array[2])
        len3 = len(data_array[3])
        len4 = len(data_array[4])
        len5 = len(data_array[5])
        len6 = len(data_array[6])
        len7 = len(data_array[7])
        len8 = len(data_array[8])
        
        #check if all lengths in data array columns the same
        
        len_pass = (len0 == len1 == len2 == len3 == len4 == len5 == len6 == len7 == len8)
        
        if len_pass == False:
            ValueError('lengths of all elements [n] in data_array must be equal')
        
        #transform and sort the array (sorting works better with rows in 1st dimension)
        data_array_np = np.array(data_array)
        data_array_transposed = np.transpose(data_array_np)
        data_array_relist = np.ndarray.tolist(data_array_transposed)
        
        if rank_by == 'group':
            data_array_sorted = sorted(data_array_relist, key = lambda x:float(x[4]))
            data_array_sorted.sort(key = lambda x:x[0])
        elif rank_by == 'score':
            data_array_sorted = sorted(data_array_relist, key = lambda x:x[0])
            data_array_sorted.sort(key = lambda x:float(x[4]))
        
        
        print('writing score_table')
        score_table_fileObj = open(score_table_path, 'a')
        first_line = 'group\tchain\tres\tsub\tmut_scr\trlx_scr\tdiff_rlx\tclean_score\tdiff_cln\n'
        newLines = ''
        
        for i in range(0,len(data_array_sorted) + 1,1):
            
            if i == 0:
                
                newLines = newLines + first_line
                
            else:
                
                line_to_add = ''
                row = data_array_sorted[i-1]
                rlx_score_diff = float(row[6])
                cln_score_diff = float(row[8])
                
                if (rlx_score_diff < rlx_diff_cutoff) & (cln_score_diff < cln_diff_cutoff) | (cutoff == False):
                
                    for n in range(0,len(row),1):
                        
                        if n < len(row)-1:
                            line_to_add = line_to_add + row[n] + '\t'
                        elif n == (len(row) - 1):
                            line_to_add = line_to_add + row[n] + '\n'
                            
                    newLines = newLines + line_to_add
                
        score_table_fileObj.write(newLines)
        
        return(data_array_sorted)
                    
def rank_muts(rosetta_scoretable_list, #output (data_array_sorted) of above function
              top_cluster_file = None, #output of 
              include_cluster_weights = False, #Do we weight the calculated averages and frequencies by 
                                              #cluster weight, i.e. the fraction of conformational space
              output_dir = os.getcwd(), #where do we write the file
              filename_prefix = 'rosetta_mutant_rankings',
              overwrite = False):
    
    #Purpose: get rankings for chain/postion/mutation combinations within
    #groups (groups of mutants generated using the same pdb structure/cluster_centroid)
    #calculate average ranking, frequency of being in the top 10 or top 25 of any group,
    #and an average change in rosetta score relative to relaxed rosetta structure.
    #Note that the averages and frequencies here are averages across groups.
    
    #Note that this function expects for every mutant structure in one group,
    #there is a structure in another group with the exact same mutation at the
    #exact same position. This is due to 
    
    #workflow: obtain list with cluster names and associated frequencies
    # separately, sort rosetta_scoretable_list by group and then by score
    #thereby ranking mutations for each group
    
    #go throuh each group, and if a new mutation encountered, add to list of mutations.
    #for the given group, find the cluster ID within the name, and map the group name
    #to the cluster ID via a regular expression match.
    
    #to the list of mutations add for a given mutation the group ID, the rank, and the change in rosetta score relative to relaxation
    
    if include_cluster_weights == True:
        
        #load in cluster list as a #clustersx#columns list
        #later in the function, we will add group names to put a mapping between
        #group names and cluster weight (%age of MD simulation population accounted for by cluster)
        
        cluster_fileobj = open(top_cluster_file,'r')
        clust_regex = 't.[0-9]*_prepped_output$' #This regex must be changed if naming of output folders changes
        sub_regex = 't.[0-9]*' #Used to find the cluster number
        cluster_list = []
    
        for line in cluster_fileobj:
            
            cluster_list.append(line.split())
        
    rosetta_scoretable_list.sort(key = lambda x:float(x[4])) #order by group, then rosetta score
    rosetta_scoretable_list.sort(key = lambda x:x[0])
    
    group_list = [] #we add groups to the list if we haven't seen them before, and for each group append mutations
    # in order of increasing rosetta score
    seen_groups = [] #list of groups we've already seen. easier to use if not in statements with than group_list
    group_index = 0 #To use for indexing group_list
    rank_num = 1 #index used to rank mutations for each group. reset after each group ends
    mut_list = [] #list of mutations. mutation is first element of each row (chain:RES:NCAA)
                #, and subsequent elements are [group, rank,  change in rosetta score due to mutation]
                #with each element's contents corresponding to a group and the associated rank
                #and change in rosetta score
    seen_muts = [] #list of mutants we've already seen. going through the file, we add a mutant to the list if we haven't seen it before
    mut_stat_list = [] #to contain statistics regarding mutant ranking and change in rosetta score
    
                    
    
    for i in range(len(rosetta_scoretable_list)):
        
        row = rosetta_scoretable_list[i]
        indexed_group = rosetta_scoretable_list[i][0]
        
        if indexed_group not in seen_groups:
            
            group_list.append([indexed_group])
            seen_groups.append(indexed_group)
            
            if include_cluster_weights == True: #WATCH THE CODE within
                
                #NOTE: ideally should have something here to check that number 
                #of matches for each regular expression = 1 and only 1.
                match1 = re.search(clust_regex, indexed_group)
                
                if type(match1) == None:
                    raise ValueError('check cluster list')
                    
                group_name_substring = match1.group(0) #returns the matched substring
                match2 = re.search(sub_regex, group_name_substring)
                group_clust_ID = match2.group(0) #cluster ID for group, corresponds to cluster from which mutants derived
                
                for n in range(len(cluster_list)):
                    
                    if cluster_list[n][0] == group_clust_ID:
                        
                        cluster_list[n].append(indexed_group)
                        break
            
        
        #Below if/else statement determines whether or not we've reached the end
        #of a group. If this is the case, i will be the length of the scoretable list -1
        #or the indexed group will not be equal to the group listed in the next row of
        #the scoretable list. If group_end is true;
        if (i == len(rosetta_scoretable_list)-1):
            
            group_end = True
            
        elif indexed_group != rosetta_scoretable_list[i+1][0]:
            
            group_end = True
            
        else:
            
            group_end = False
            
        mut = str(row[1]) + str(row[2]) + str(row[3])
        
        #If we haven'tseen the mutation before, add it to the list of mutations
        #mutation defined as Chain/Residue#/NCAA combination
        if mut not in seen_muts:
            
            mut_list.append([mut])
            seen_muts.append(mut)

        group_list[group_index].append(mut)
        
        d_relax = row[6] #change in rosetta score for the given mutant
        
        #Find mutation in mut_list, and append the indexed group and rank_num and d_relax  as a tuple to
        #the approrpiate mutation in mut_list
        for n in range(len(mut_list)):
            
            if mut_list[n][0] == mut:
                
                
                mut_list[n].append((indexed_group,rank_num,d_relax))
                break
                
            elif n == (len(mut_list)-1):
                
                raise ValueError('could not find mutation ' + mut + ' in mut_list. This should not be possible. Check function for bugs.')
                
        rank_num = rank_num + 1 #increment rank number by 1. Reset to 1 if we reach end of a group
        
        
        if group_end == True:
            
            rank_num = 1 #reset rank number
            group_index = group_index + 1 #increment group index
        
    #Now that we have a group list and a mut list, we shall do some calculations
    #to find the frequency in the top 10 and top 25 of each group,
    #and the average of change in rosetta score,
    
    for i in range(len(mut_list)):
        
        #For random variable X, add Xi to the list for each group i
        #Expected value of X will be calculated, with P(Xi) == 1/n groups 
        #if we are not weighting by cluster frequency, P(Xi) == cluster frequency
        # (i.e. the fraction of the population of MD structures that cluster accounts for)
        #if we are. Variance calculated by going through each list, then 
        #calculating (Xi-E[X])^2*P(Xi), then adding it to the appropriate variance
        #variable
        mut_row = mut_list[i]
        
        avg_drelax = 0.0
        avg_rank = 0.0
        freq_t10 = 0.0
        freq_t25 = 0.0
        drelax_list = []
        rank_list = []
        freq_t10_list = []
        freq_t25_list = []
        prob_list = [] #probability list
                    
        for n in range(1,len(mut_row),1):
            
            #Mut row should look like [mutant,(group,rank,drelax),(group,rank,drelax),...]
            d_relax_i = float(mut_row[n][2])
            rank_i = float(mut_row[n][1])
            drelax_list.append(d_relax_i)
            rank_list.append(rank_i)
            
            #Degermine if mutation is in top 10 or top 25 for the group
            if rank_i <= 10:
                
                in_t10 = 1
                
            else:
                
                in_t10 = 0
                
            if rank_i <= 25:
                
                in_t25 = 1
                
            else:
                
                in_t25 = 0
                
            freq_t10_list.append(in_t10)
            freq_t25_list.append(in_t25)
            
            #In below if/else statement, set the value of pXi, the probability of viewing Xi.
            #Equal to 1/(number of groups) if not weighting by cluster.
            #Equal to group's cluster's fraction of MD structure population if we are.
            
            if include_cluster_weights == True: #If we are weighting by cluster weight
                
                group = mut_row[n][0] #group corresponding to the given rank and d_relax stats
                
                for m in range(0, len(cluster_list),1):
                    
                    clust_row = cluster_list[m]
                    
                    if group == clust_row[len(clust_row)-1]:
                        
                        pXi = float(clust_row[2]) #retrieve cluster weight
                        
                        break
                    
                    elif m == (len(cluster_list)-1):
                        
                        raise ValueError('could not find group in cluster list. code may be broken')
                        
            else:
                
                pXi = 1/(len(mut_row) - 1) #otherwise pXi = 1/#groups
                
            
            prob_list.append(pXi)
            
            avg_drelax = avg_drelax + d_relax_i*pXi
            avg_rank = avg_rank + rank_i*pXi
            freq_t10 = freq_t10 + in_t10*pXi
            freq_t25 = freq_t25 + in_t25*pXi
        
        
        #Now we calculate the variance
        var_drelax = 0.0
        var_rank = 0.0
        var_t10_freq = 0.0
        var_t25_freq = 0.0
        
        for n in range(0,len(mut_row)-1,1):
            
            #Set up variables Xi, Yi, ...
            drlx_i = drelax_list[n]
            rnk_i = rank_list[n]
            t10_i = freq_t10_list[n]
            t25_i = freq_t25_list[n]
            prob_Xi = prob_list[n]
            
            #add to variance variable
            var_drelax = var_drelax + ((drlx_i - avg_drelax)**2)*prob_Xi
            var_rank = var_rank + ((rnk_i - avg_rank)**2)*prob_Xi
            var_t10_freq = var_t10_freq + ((t10_i - freq_t10)**2)*prob_Xi
            var_t25_freq = var_t25_freq + ((t25_i - freq_t25)**2)*prob_Xi
            
        #Since mut_list and mut_stat_list had mutants appended in same order,
        #can just use i to index correct mutant for which we have just calculated
        #the stats
        mut_ID = mut_row[0]
        mut_stat_row = [mut_ID, 
                        avg_drelax, 
                        var_drelax,
                        avg_rank,
                        var_rank,
                        freq_t10,
                        var_t10_freq,
                        freq_t25,
                        var_t25_freq]
        
        mut_stat_list.append(mut_stat_row)
        
    #Now that we've made the group rankings and the stat list, it's time to 
    #write these things to two separate files
    
    #change directory
    default_dir = os.getcwd()
    os.chdir(output_dir)
    
    #Check that number of elements in grouplist[i] equilevant for grouplist[i]  0 <= i <= len(grouplist)-1
    #If this is not the case, raise an error. If this condition fails and we proceed any further
    #the file written will have the mutant rankings scrambled.
    
    target_length =  len(group_list[0])
    print(group_list)
    for i in range(0,len(group_list)):
        print(group_list[i])
        print(len(group_list[i]))
        
        if len(group_list[i]) != target_length:
            
            raise ValueError('dimensions of group_list variable incorrect. This could\
 be caused by mutations in one group not appearing in another. This function expects that\
 for every mutant structure in one group, there will be another with the exact same mutation\
 at the exact same position in every other group. Check your rosetta_scoretable tsv file to\
 see if this is the case.')
    
    #This won't change the order of the rankings, will just change the 'rows' and 'columns' of the list
    #such that we'll have a much easier time writing the file
    group_rank_array = np.array(group_list)
    group_ranks_transposed = np.transpose(group_rank_array)
    group_ranks_relist = np.ndarray.tolist(group_ranks_transposed)
    
    #give filenames
    group_rank_filename = filename_prefix + '_group_rankings.tsv'
    mut_stats_filename = filename_prefix + '_mut_stats.tsv'
    
    #write the group_rank file
    
    #below if else statement just checks if a file exists and whether or not to overwrite it
    if os.path.exists(group_rank_filename):
        
        if overwrite == True:
            
            os.remove(group_rank_filename)
            
        else:
            
            raise ValueError(group_rank_filename + ' already exists in given output directory ' + output_dir + 
                             '. Please choose another filename prefix or specify overwrite to True.')
    
    group_rank_fileobj = open(group_rank_filename,'a')
    group_rank_filestr = ''
    
    for i in range(len(group_ranks_relist)):
        
        newStr = ''
        
        if i == 0: #If first row, write rank in first column
            
            newStr = newStr + 'RANK\t'
            
        else: #otherwise write rank number
            
            newStr = newStr + str(i) + '\t' 
        
        group_ranks_row = group_ranks_relist[i]
        
        for n in range(len(group_ranks_row)):
            
            newEl = str(group_ranks_row[n]) + '\t'
            newStr = newStr + newEl
            
        newStr = newStr + '\n'
        
        group_rank_filestr = group_rank_filestr + newStr
    #Now that we have the file string, write it to the group rank file   
    group_rank_fileobj.write(group_rank_filestr)
    group_rank_fileobj.close()
    
    #Now we write the mutation stats file
    
    #below if else statement just checks if a file exists and whether or not to overwrite it
    if os.path.exists(mut_stats_filename):
        
        if overwrite == True:
            
            os.remove(mut_stats_filename)
            
        else:
            
            raise ValueError(mut_stats_filename + ' already exists in given output directory ' + output_dir + 
                             '. Please choose another filename prefix or specify overwrite to True.')
    
    mut_stats_fileobj = open(mut_stats_filename,'a')
    
    #below, gives a header for the tsv file
    mut_stats_filestr = ('mutation\tavg_drelax\tvar_drelax\tavg_rank\tvar_rank' +
                        '\tfreq_t10\tvar_t10_freq\tfreq_t25\tvar_t25_freq\n')
    
    #put in the rest of the info
    for i in range(len(mut_stat_list)):
        
        newString = ''
        mut_row = mut_stat_list[i]
        
        for n in range(len(mut_row)):
            
            newString = newString + str(mut_row[n]) + '\t'
            
        newString = newString + '\n'
        
        mut_stats_filestr = mut_stats_filestr + newString
    
    #and write the complete string to file    
    mut_stats_fileobj.write(mut_stats_filestr)
    mut_stats_fileobj.close()
    
    os.chdir(default_dir)
    
    return(group_list,mut_stat_list)
    