import re


def parseText2(fileObj): #take a fileobject and split the lines by whitespace, returning a list
    myArray = []
    for line in fileObj:
        row = line
        myArray.append(row.split())
    return(myArray)

def parseText_RE(fileObj,regex): #take a fileobject and split the lines by a regular expression, e.g. commas (,)
    myArray = []
    for line in fileObj:
        row = line
        myArray.append(re.split(regex,row))
    return(myArray)    

