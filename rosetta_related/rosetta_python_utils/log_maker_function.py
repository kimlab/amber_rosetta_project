#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  5 13:20:00 2017

@author: owenwhitley
"""

#THIS IS TO BE REWORKED OR DISCARDED. SERVED AS METHOD FOR LOGGING
#IN THE MUTATE_AND_PACK FUNCTION AND OTHER FUNCTIONS, BUT CAN CAUSE
#A TOO MANY OPEN FILES ERROR.

import os
import logging

def make_logfile(log_file_name,operation_performed,additional_arguments):
    # create logger
    logger = logging.getLogger(operation_performed)
    logger.setLevel(logging.DEBUG)
    
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    
    fHandler = logging.FileHandler(log_file_name)
    fHandler.setLevel(logging.DEBUG)
    
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    
    # add formatter to ch
    ch.setFormatter(formatter)
    fHandler.setFormatter(formatter)
    
    # add ch to logger
    logger.addHandler(ch)
    logger.addHandler(fHandler)
    
    # 'application' code

    for i in range(0,len(additional_arguments),1):
        logger.info(additional_arguments[i])
    fHandler.flush()    
    fHandler.close()
    fHandler = []
    print (logging.Logger.manager.loggerDict.keys())
    