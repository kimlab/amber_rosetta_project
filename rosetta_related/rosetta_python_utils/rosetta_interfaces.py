#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 09:05:38 2017

@author: kimadmin
"""
import findFiles as FF
import os
import subprocess
import resFileUtilsV2 as RFU
import log_maker_function as lmf
import parseText as pt
import clean_pdb as CP
import time
import numpy
from multiprocessing import Process
#ROSETTA INTERFACES ===========================================================

def relax_pdb(clean_pdb_path, #filepath to clean pdb file
              resfile_path,  #filepath to input resfile
              rosetta_scripts_executable, #filepath to rosetta_scripts executable file
              rosetta_scripts_xml, #xml giving instructions for the relax protocol
              output_dir, #output directory for the relaxed pdb (note, put in name, not full path)
              output_dir_parent): #full path to parent directory of output_dir
    
    #overview: given clean pdb file, resfile for relaxation, and a relax protocol
    #as an xml script, start a subprocess of rosetta scripts using these inputs,
    #so that we get a relaxed pdb and a corresponding scorefile as an output


    default_dir = os.getcwd()
    
    newDir = os.path.join(output_dir_parent, output_dir)
    
    if os.path.exists(newDir) == False:
        os.mkdir(newDir)
        
        
    #After making directory, construct the command
    os.chdir(newDir)
    command = (rosetta_scripts_executable + 
               " -in:file:s " + 
               clean_pdb_path +
               " -parser:protocol " + 
               rosetta_scripts_xml +
               " -parser:script_vars resFile=" + 
               resfile_path)
    command_split = command.split()
    
    #run the command
    subprocess.run(command_split)
    
#    #If logging, make log file
#    if logfile == True:
#        logArgs = []
#        logArgs.append("====================================")
#        logArgs.append("created relaxed PDB structure")
#        logArgs.append("input pdb file: " + clean_pdb_path)
#        lmf.make_logfile(logfile_name,"relax",logArgs)
    
    os.chdir(default_dir) #reset working directory 
    
    
    

#==============================================================================    
def score_pdb(clean_pdb_path, rosetta_scripts_executable, rosetta_scripts_xml,
              output_dir):
    
    #given a filepath to a pdb, score the pdb, and generate a scorefile and pdb
    #with pairwise interaction scores. Procedure is to make a directory for output,
    #go to said directory, and start a rosetta scripts subprocess using
    #the clean pdb and the rosetta scripts xml containing the scoring protocol.
    
    default_dir = os.getcwd()
    
    if os.path.exists(output_dir) == False:
        os.mkdir(output_dir)
        
    #After making directory, construct the command
    
    os.chdir(output_dir) #changing directory. Rosetta likes to put output in wd
    
    command = (rosetta_scripts_executable + 
               " -in:file:s " + 
               clean_pdb_path +
                " -parser:protocol " + 
                rosetta_scripts_xml +
                " -out:prefix score_")
    command_split = command.split()
    
    subprocess.run(command_split)
    
    os.chdir(default_dir) #reset working directory
    
#==============================================================================

def mutate_and_pack(relaxed_pdb_path, #path to relaxed pdb
                    rosetta_scripts_executable, #path to rosetta scripts executable
                    rosetta_scripts_xml, #xml file containing the packing protocol
                    output_dir, #name (not path) of output directory
                    output_dir_parent, #path of parent directory of output directory
                    resfile_path, #path to appropriate resfile template for 
                                    #packing protocol. Note that this serves as a template,
                                    #and a separate temproary resfile will be created
                                    #for each mutation that occurs.
                    ncaa_list_path, #path to list of ncaas with which to mutate selected residues
                                    #file should be a text file with each line as <NCAA_name>
                    residue_list, #file containing list of chain/position locations to mutate. 
                                #should be formatted with each line containing <chain>\t<residue>
                    mut_res_commands = ['EX 1', 'EX 2'], #each resfile used to generate a mutation will be identical to the packing resfile,
                                                         #except that at the position to by mutated the command is
                                                         # <residue#> <chain> EMPTY NC <non-canonical amino acid> mut_command[0] mut_command [1] etc.
                                                         #for example, part of a resfile used to mutate residue 144 on chain A of a protein might
                                                         #look like:
                                                         #  143 A NATAA EX 1
                                                         #  144 A EMPTY NC A81 EX 1 EX 2
                                                         #  145 A EMPTY EX 1
                                                         #
                                                         #In this case, pack_commands would have been set to ['EX 1'],
                                                         # and mut_commands would have been set to ['EX 1', 'EX 2']
                    suffix = "", #Not used in function. should likely be done away with when logging procedure
                    #is redone
                    logfile = False): #logfile must be set to false
    
    
                                    #logging will occur in a predetermined fashion
                                    # logfile goes to output directory, logfile name
                                    #determined by name of output directory
                                    
                                    #note: since putting output into existing directories
                                    # is forbidden, no need to check that a logfile
                                    # already exists
    
    #GIVEN A LIST OF RESIDUES AND A LIST OF NON CANONICAL AMINO ACIDS
    #MAKE MUTATED STRUCTURES AT ALL SPECIFIED POSITIONS INCORPORATING EACH
    #ONE OF THE NON CANONICAL AMINO ACIDS.
    # For a given PDB, the mutate and pack function will
    # generate a mutant for every possible combination of <chain> <residue#>
    # and <non-canonical AA> given the chain/residue pairs you specify in 
    # the residuel_list file and the list of non-canonical amino acids you provide.
    # Basically, this function works by taking the resfile you give it,
    # the positions you want to perform mutation at, and the list of possible mutant residues,
    # and for each combination of position/NCAA, generate a resfile specifying
    # a mutation at the given position to the given NCAA. It then constructs
    # a command to run via subprocess.run to generate the mutant via rosetta scripts,
    # and once the subprocess completes, moves on to the next position/NCAA combination
    # and repeats the process.
    
    
    #NOTE: THE METHOD OF LOGGING FOR THIS FUNCTION NEEDS TO BE REWORKED ENTIRELY.
    #logfile MUST BE SET TO FALSE
    
    #Give logfile a name. Doesn't mean we're making it necessarily    
    logfile_name = output_dir + "_process_logfile.txt"
    
    
    
    default_dir = os.getcwd() #store current wd in a variable
            
    
    newDir = os.path.join(output_dir_parent, output_dir) 
   
    if os.path.exists(newDir) == False:
        os.mkdir(newDir)
    else:
        raise ValueError("Directory already exists")
        
    #Switch to new directory
    
    os.chdir(newDir) #changing directory. Rosetta likes to put output in wd

    #If we're writing logfile, write some lines detailing inputs
    if logfile == True:
        
        
        logArgs = []
        logArgs.append("=============================================")
        logArgs.append("BEGIN mutate_and_pack function")
        logArgs.append("pdb file: " + relaxed_pdb_path)
        logArgs.append("rosetta_scripts executable: " + rosetta_scripts_executable)
        logArgs.append("output directory full path: " + os.path.join(output_dir_parent, output_dir))
        logArgs.append("resfile filepath: " + resfile_path)
        logArgs.append("ncaa list filepath: " + ncaa_list_path)
        logArgs.append("mutant residue commands: " + str(mut_res_commands))
        logArgs.append("residues selected for mutation: " + str (residue_list))
        lmf.make_logfile(logfile_name, "mutate_and_pack", logArgs)
        
    
    #Load NCAA List into a list object
    ncaa_file_obj = open(ncaa_list_path,'r')
    ncaa_list = RFU.parseText2(ncaa_file_obj)
    
    #Load resfile array as a template for editing (we'll make lots of 
    # temporary resfiles)
    resFileArray = RFU.resfile_to_resFileArray(path = resfile_path,
                                               logfile = logfile,
                                               logfile_dir = newDir,
                                               logfile_name = logfile_name,
                                               logfile_append = True,
                                               logIndicator = str(0))
    
    original_suffix = suffix #save original suffix
    
    for i in range(0,len(residue_list),1):
        
        #expecting an i x 2 list with each row i containing entries
        #for residue number and chain
        resNum = residue_list[i][0]
        chain = residue_list[i][1]
        res_chain_dir = newDir + "/chain_" + chain + "_res_" + str(resNum)
        os.mkdir(res_chain_dir) #This line and line above added to
        #make folder for each residue at each chain where changes occured
        #Stores output pdbs and scorefiles
        
        os.chdir(res_chain_dir) #change working directory to route output
                                #to above created folder
        
        for n in range(0,len(ncaa_list),1):
            
            if logfile == True: #If making logfile, log this info
                os.chdir(newDir)
                logArgs1 = []
                logArgs1.append("======================================")
                logArgs1.append("ITERATION " + str(n + 1))
                logArgs1.append("creating edited set of commands")
                lmf.make_logfile(logfile_name, "edit_commands_" + str(n+1), 
                                 logArgs1)
                del logArgs1
                os.chdir(res_chain_dir)
            
            #for each ncaa, specify ncaa, create a modified resfile,
            #and input said resfile into rosetta scripts
            
            #select ncaa from list
            ncaa = ncaa_list[n][0]
            
            #construct commands for single residue editing, based on chain
            #residue number, and ncaa selected
            edit_commands = ['EMPTY', 'NC', ncaa] + mut_res_commands
            
            #create an edited array of resfile info, identical to
            #that contained in original resfile but with the specified
            #residue mutated. Include additional commands (mut_res_commands)
            single_res_edited = RFU.changeCommSingle(resFileArray = resFileArray, 
                                           chain = [chain], 
                                           position = resNum,
                                           Commands = edit_commands,
                                           logfile = logfile,
                                           logfile_dir = newDir,
                                           logfile_name = logfile_name,
                                           logfile_append = True,
                                           logIndicator = str(n+1))
            #create the new resfile (a temporary file)
            
            if logfile == True:
                os.chdir(newDir)
                logArgs2 = []
                logArgs2.append("=======================================")
                logArgs2.append("creating temporary resfile")
                lmf.make_logfile(logfile_name, "make_temp_resfile_" + str(n+1),
                                 logArgs2)
                del logArgs2
                os.chdir(res_chain_dir)
                
            resFileName = "temp.resfile"
            RFU.array2ResFile(single_res_edited, 
                              res_chain_dir, 
                              resFileName,
                              logfile = logfile,
                              logfile_dir = newDir,
                              logfile_name = logfile_name,
                              logfile_append = True,
                              logIndicator = str(n+1))
            
            #above line edited 6.5.17 
            suffix = (suffix + "_residue_" + str(resNum) + "_chain_" +
                      chain + "_sub_" + ncaa)
            
            if logfile == True:
                
                os.chdir(newDir)
                logArgs3 = []
                logArgs3.append("=======================================")
                logArgs3.append("executing command in rosetta scripts")
                lmf.make_logfile(logfile_name, "mutate_and_pack_exec_command" + str(n+1), logArgs3)
                del logArgs3
                os.chdir(res_chain_dir)
                
            
            #Construct the command for rosetta
            rosetta_command = (rosetta_scripts_executable + 
                               " -in:file:s " +
                               relaxed_pdb_path + 
                               " -parser:protocol " +
                               rosetta_scripts_xml +  
                               " -parser:script_vars resFile=" +
                               "temp.resfile" + 
                               " -out:suffix " +
                               suffix)
            
            rosetta_command_split = rosetta_command.split()
            
            #Run the command in rosetta_scripts
            subprocess.run(rosetta_command_split)
            
            #remove resfile to avoid taking up space on HD or other storage
            
            if logfile == True:
                os.chdir(newDir)
                logArgs4 = []
                logArgs4.append("=======================================")
                logArgs4.append("removing temporary resfile")
                lmf.make_logfile(logfile_name, "remove_temp_resfile_" + str(n+1), logArgs4)
                del logArgs4
                os.chdir(res_chain_dir)
            
            os.remove(os.path.join(res_chain_dir,"temp.resfile"))
            
            suffix = original_suffix
            
            
            
        
    os.chdir(default_dir) #change working directory in python back to default
    
    if logfile == True:
        
        os.chdir(os.path.join(output_dir_parent,output_dir))
        
        logArgs5 = []
        logArgs5.append("=========================================")
        logArgs5.append("Finished program")
        lmf.make_logfile(logfile_name, "finished", logArgs5)
    
        os.chdir(default_dir)
        
                            
#==============================================================================

def mut_pack_pdb_seq(pdbList, #List of pdb file names
                     #Note that we pass a list of pdb names to allow us to pass subsets
                     #of the folder contents into this function
                     pdbFolder, #folder in which pdbs located
                     rosetta_scripts_executable, #Filepath of the rosetta scripts executable
                     rosetta_scripts_xml_relax, #XML you'd like to use for relax protocol
                     rosetta_scripts_xml_pack, #XML you'd like to use for the mutate and pack protocol
                     rosetta_scripts_xml_score,
                     parent_parent_dir, #parent dir for all output directories
                     ncaa_list_path, #path to ncaa_list file. each line of format <ncaa_name>
                     residue_list_path, #path to residue list. each line of file should be <chain>\t<residue#>.
                                         #each combinatino of chain/residue specifies a residue# on a given chain to mutate
                     relax_commands = ['EX 1', 'EX 2'], #commands for the relax protocol.
                     #resfile will have for each line <residue#> <chain> NATAA relax_commands[0] ... relax_commands [N]
                     #as the commands given. For example, these 'relax_commands'
                     #would give a resfile line similar to 1 A NATAA EX 1 EX 2 for each line.
                     #note that you must type the commands exactly as you would when manually editing a resfile
                     pack_commands = ['EX 1', 'EX 2'],
                     #same as relax_commands, but applied to the packing resfile which serves as a template
                     #for the resfiles used to generate mutants. Note that there is no standalone packing step
                     #with no mutation in this function. Instead, the packing resfile is used as a template
                     #for the resfiles used to generate mutants.
                     #rather this serves as a template for the resfiles used to generate mutants.
                     mut_commands = ['EX 1', 'EX 2']
                     #each resfile used to generate a mutation will be identical to the packing resfile,
                     #except that at the position to by mutated the command is
                     # <residue#> <chain> EMPTY NC <non-canonical amino acid> mut_command[0] mut_command [1] etc.
                     #for example, part of a resfile used to mutate residue 144 on chain A of a protein might
                     #look like:
                     #  143 A NATAA EX 1
                     #  144 A EMPTY NC A81 EX 1 EX 2
                     #  145 A EMPTY EX 1
                     #
                     #In this case, pack_commands would have been set to ['EX 1'],
                     # and mut_commands would have been set to ['EX 1', 'EX 2']
                     ):
    
    #Context: this function can take a list of 'prepped' pdbs from amber/clustering
    #and, given the rest of the inputs to this function, make mutant structures via
    #rosetta. For each pdb in the list, it runs through the entire process of 
    #cleaning the pdb, relaxing it, and then generating and packing mutant (single mutant) structures
    
    #General workflow: from a list of pdbs, from folder, go through each pdb name
    # for each pdb, make an output folder that is a subdirectory of the 'parent
    #parent dir' (parent dir for all pdb output folders) and go through the process
    # of making a clean pdb from the raw pdb, making a raw resfile from the clean pdb,
    # making two edited resfiles depending on desired commands for relax and pack
    # then run the relax protocol with the relax resfile and clean pdb as input
    # Afterwards, take the resulting relaxed pdb, the list of residues you want to mutate
    # and the list of NCAAs you want to use, and the pack resfile as inputs 
    # to the mutate and pack function. For a given PDB, the mutate and pack function will
    # generate a mutant for every possible combination of <chain> <residue#>
    # and <non-canonical AA> given the chain/residue pairs you specify in 
    # the residuel_list file and the list of non-canonical amino acids you provide.
    
    # Note that the mutate and pack function generates
    # for each mutant a resfile tailored to effect a mutation in rosetta using the pack_resfile
    # as a template, and then uses the 'tailor made' resfile and the packing
    # protocol xml as input for rosetta scripts. The tailor made/mutant resfile
    # is deleted after rosetta scripts generates the mutant and finishes packing
    
    completion_times = []
    
    for i in range(0,len(pdbList),1):
        
        start_time = time.time()
        
        pdbName = pdbList[i] #name for pdb
        
        output_folder_name = pdbName[0:-4] + '_output'
        output_dir = os.path.join(parent_parent_dir, output_folder_name)
        clean_pdb_dir = os.path.join(output_dir,'cleaned_pdbs') #directory for clen pdbs
        raw_resfile_dir = os.path.join(output_dir,'raw_resfile')
        mod_resfile_dir = os.path.join(output_dir,'mod_resfile')
        relaxed_pdb_dir = os.path.join(output_dir,'relaxed_pdb')
        
        
        os.mkdir(output_dir)
        os.mkdir(clean_pdb_dir)
        os.mkdir(raw_resfile_dir)
        os.mkdir(mod_resfile_dir)
        os.mkdir(relaxed_pdb_dir)
        
        
        raw_pdb_path = os.path.join(pdbFolder,pdbName) #complete path to pdb
        
        CP.clean_pdb(filePath = raw_pdb_path, #path to pdb file
                      keepLines = ["ATOM","TER"], #lines in pdb to keep
                      suffix = "_clean",
                      parent_dir = output_dir, #parent directory of destination folder
                      destPath = "cleaned_pdbs", #name of destination folder
                      overwrite = False, #overwrite existing file?
                      new_directory = False, 
                      logfile = False)
        
        clean_pdb_name = pdbName[0:-4] + '_clean.pdb'
        clean_pdb_path = os.path.join(clean_pdb_dir,clean_pdb_name)
        
        score_pdb(clean_pdb_path,rosetta_scripts_executable,rosetta_scripts_xml_score,clean_pdb_dir)
        raw_resfile_name = pdbName[0:-4] + '_raw_resfile.resfile'
        
        RFU.make_resfile(pdbPath = clean_pdb_path, #filepath to pdb file you wish to generate resfile for
                         resfile_name = raw_resfile_name, #name for resfile
                         destPath = 'raw_resfile', #enter a string here for the destination folder (not a full directory path)
                         parent_directory = output_dir, #parent directory of destination
                         new_directory = False, #boolean. Do we make a new directory?
                         overwrite = False, #boolean. do we overwrite an existing resfile?
                         Use_Input_SC = True, #boolean. If true, will write USER_INPUT_SC to first line
                                         #meaning that rosetta will allow experimentally determined rotamers
                                         #in the output structure it generates (if using packer)
                         allowables = ["ATOM","TER"], #which lines do we allow? format as a list with string elements
                         logfile = False,
                         logIndicator = "")
        
        raw_resfile_path = os.path.join(raw_resfile_dir,raw_resfile_name)
        relax_mod_resfile_name = pdbName[0:-4] + '_relax_mod.resfile'
        relax_mod_resfile_log_name = relax_mod_resfile_name[0:-8] + '_log.txt'
        relax_mod_resfile_path = os.path.join(mod_resfile_dir,relax_mod_resfile_name)
        
        RFU.edit_resfile(source_resfile = raw_resfile_path, #path to resfile, full or relative
                         output_dir = mod_resfile_dir, #Note: This must be a full directory path, not the name of a subdirectory
                         new_directory = False, 
                         fName = relax_mod_resfile_name, 
                         overwrite_resfile = False,
                         edit_mode = 'chains', 
                         chains = ['ALL'], 
                         position = None,
                         AAstart = None, #if specified range, AA start specifies first residue of range, AAend specifies last
                         AAend = None, 
                         Commands = relax_commands,
                         logfile = False, 
                         logfile_name = relax_mod_resfile_log_name, #Note that the logfile is constructed by appending logfile info
                                             #from multiple operations (resfile to array, change commands, array to resfile)
                                             #Thus logfile_append true for all functions called within this function
                                             #that make a logfile. 
                         logfile_append = False, #If logfile append = false, make another logfile for the entire process
                         logIndicator = "")
        
        pack_mod_resfile_name = pdbName[0:-4] + '_pack_mod.resfile'
        pack_mod_resfile_log_name = pack_mod_resfile_name[0:-8] + '_log.txt'
        pack_mod_resfile_path = os.path.join(mod_resfile_dir, pack_mod_resfile_name)
        
        RFU.edit_resfile(source_resfile = raw_resfile_path, #path to resfile, full or relative
                         output_dir = mod_resfile_dir, #Note: This must be a full directory path, not the name of a subdirectory
                         new_directory = False, 
                         fName = pack_mod_resfile_name, 
                         overwrite_resfile = False,
                         edit_mode = 'chains', 
                         chains = ['ALL'], 
                         position = None,
                         AAstart = None, #if specified range, AA start specifies first residue of range, AAend specifies last
                         AAend = None, 
                         Commands = pack_commands,
                         logfile = False, 
                         logfile_name = pack_mod_resfile_log_name, #Note that the logfile is constructed by appending logfile info
                                             #from multiple operations (resfile to array, change commands, array to resfile)
                                             #Thus logfile_append true for all functions called within this function
                                             #that make a logfile. 
                         logfile_append = False, #If logfile append = false, make another logfile for the entire process
                         logIndicator = "")
        
        
        
        relax_pdb(clean_pdb_path = clean_pdb_path, 
                  resfile_path = relax_mod_resfile_path, 
                  rosetta_scripts_executable = rosetta_scripts_executable,
                  rosetta_scripts_xml = rosetta_scripts_xml_relax, 
                  output_dir = 'relaxed_pdb', #refers to the directory where relaxed pdb is put
                  output_dir_parent = output_dir) #output_dir in this instance refers to the pdb specific parent directory
                                                  #which contains all outputs for a given pdb. it is the parent of relaxed_pdb_dir
                                                  
        relaxed_pdb_name = clean_pdb_name[0:-4] + '_0001.pdb'
        relaxed_pdb_path = os.path.join(relaxed_pdb_dir, relaxed_pdb_name)
        

        residue_list_fileObj = open(residue_list_path, 'r')

        residue_list = pt.parseText2(residue_list_fileObj)

        #below: an ugly way to make sure everything in column 1 of the residue list is of type integer
        for i in range(0,len(residue_list),1):
            residue_list [i][0] = int(residue_list[i][0])
        
        mutate_and_pack(    relaxed_pdb_path = relaxed_pdb_path, 
                            rosetta_scripts_executable = rosetta_scripts_executable,
                            rosetta_scripts_xml = rosetta_scripts_xml_pack,
                            output_dir = 'mutant_structures', 
                            output_dir_parent = output_dir,
                            resfile_path = pack_mod_resfile_path, 
                            ncaa_list_path = ncaa_list_path, 
                            residue_list = residue_list,
                            mut_res_commands = mut_commands, 
                            suffix = "",
                            logfile = False)        
                
        end_time = time.time()
        completion_times.append(end_time-start_time)
        
    print(completion_times)

#==============================================================================

def multiprocess_rosetta_mut(pdb_folder, #folder containing pdb files 'prepped' after clustering. see prep from rosetta for more
                             #on this procedure
                             input_dir, #location of input files, including resdue_list, 
                             #and (if not using defaults) ncaa_list, any non-standard rosetta_scripts
                             #xml protocols
                             output_dir, #directory under which all outputs shall be contained.
                             #see comment on Outputs for more details
                             rosetta_scripts_exe, # path to rosetta scripts executable
                             residue_list, # path to text file containing list of chain and residue number identifiers
                                         # for residues you'd like to mutate. Each line in file should be formatted
                                         #<chain>\t<residue#>.
                             amber_rosetta_proj_dir, #location of amber rosetta python modules and necessary data.
                                                     #contains rosetta related folder, which will contain
                                                     #the modules and data necessary to run the function
                             
                             num_processes = 1, #how many subprocesses you want to spawn.
                                                 #basically, we divide the pdbs into num_processes
                                                 #groups and assign each group to a separate process,
                                                 #where each group member is sequentially put through the procedure
                                                 #to relax structure and generate mutants
                             relax_protocol = None, #if None, defaults to a default protocol. Otherwise, give filename
                                                     #of rosetta_scripts xml protocol for relaxing. Non-default file should be in 
                                                     #input_dir
                             pack_protocol = None, #if None, defaults to a default protocol. Otherwise, give filename
                                                     #of rosetta_scripts xml protocol for packing. Non-default file should be in 
                                                     #input_dir
                             score_protocol = None,#if None, defaults to a default protocol. Otherwise, give filename
                                                     #of rosetta_scripts xml protocol for scoring. Non-default file should be in 
                                                     #input_dir
                             relax_commands = None, #If none, defaults to the default commands in mut_pack_pdb_seq. Otherwise,
                                                     #enter as a list in a similar manner as you would for mut_pack_pdb_seq.
                                                     #again, these resfile commands are appended to all lines in the relax resfile
                                                     #as described in mut_pack_pdb_seq.
                             pack_commands = None,  #if None, defaults to default commands in mut_pack_pdb_seq.Otherwise,
                                                     #enter as a list in a similar manner as you would for mut_pack_pdb_seq.
                                                     #again, these resfile commands are appended to all lines in the pack resfile
                                                     #as described in mut_pack_pdb_seq.
                             mut_res_commands = None, #if None, defaults to default commands in mut_pack_pdb_seq.Otherwise,
                                                     #enter as a list in a similar manner as you would for mut_pack_pdb_seq.
                                                     #again, as in mut_pack_pdb_seq, these resfile commands are appended to 
                                                     #<residue#><chain> EMPTY NC <NCAA_name> for a residue that gets mutated.
                             ncaa_list = None       #list of ncaa residues you want to use for mutation. Each line in file 
                                                     # formatted as <NCAA_name> If None,
                                                     #defaults to a default list. Otherwise, enter as the name of the
                                                     #ncaa list file, which must be located in inputs directory
                             ):
    #Purpose: take a list of pdb files from a pdb folder, split that list into
    # (num_processes) smaller lists that are fed to separate processes
    # that sequentially go through each pdb file and, for each pdb file, generate an ensemble of mutant structures
    #  and associated score files
    
    #Outputs: Under output_dir, a folder corresponding to each input pdb. Under each
    # of those folders, subfolders named 'pdb_name_chainX_res_Y', giving the
    # name of the source pdb, and the chain and residue mutated for the structures in
    # a given subfolder. Under the subfolder, should find a series of pdb files
    # named pdb_name_chainX_res_Y_sub_NCAA, with chain X and RES Y corresponding
    #to residue mutated, and NCAA corresponding to the NCAA substituted at that
    #position.
    
    
    rosetta_folder = os.path.join(amber_rosetta_proj_dir,'rosetta_related')
    
    #setup the rosetta scripts protocols. If nothing given, resort to default
    #protocols located in '*amber_rosetta_project/rosetta_related/rosetta_scripts_xmls'
    xml_folder = os.path.join(rosetta_folder,'rosetta_scripts_xmls')
    
    if relax_protocol == None:
        relax_protocol = os.path.join(xml_folder, 'rosetta_fast_relax.xml')
    else:
        relax_protocol = os.path.join(input_dir,relax_protocol)
    
    
    if pack_protocol == None:
        pack_protocol = os.path.join(xml_folder, 'rosettaPack.xml')
    else:
        pack_protocol = os.path.join(input_dir, pack_protocol)
        
    if score_protocol == None:
        score_protocol = os.path.join(xml_folder, 'rosettaScore_T2014.xml')
    else:
        score_protocol = os.path.join(input_dir, score_protocol)
    
    #get ncaa_list file. If nothing specified in inputs, resort to default
    #ncaa list located in 'amber_rosetta_project/rosetta_related/NCAA_list'
    
    ncaa_list_file = ncaa_list
    
    if ncaa_list_file == None:
        ncaa_list_folder = os.path.join(rosetta_folder, 'NCAA_list')
        ncaa_list = os.path.join(ncaa_list_folder, 'NCAA_list.txt') #default NCAA list if none specified
        
    else:
            ncaa_list = os.path.join(input_dir,ncaa_list_file)
    
    residue_list_filepath = os.path.join(input_dir,residue_list)
    
    if relax_commands == None:
        relax_commands = ['NATAA','EX 1', 'EX 2']
    
    
    if pack_commands == None:
        pack_commands = ['NATAA','EX 1', 'EX 2']
        
    
    if mut_res_commands == None:
        mut_res_commands = ['EX 1', 'EX 2']
        
    np = num_processes
    
    
    #Now to the task of dividing the group of pdbs into equally or near equally sized groups for np processes.
    #Basically, we divide M pdbs by np processes to get int(M/np) sized lists.
    #If M/np is not an integer, we first put int(M/np) filenames in each list,
    #and with the remaining filenames allocate them one per list until no pdb filenames remain
    #With the pdbfiles organized into subgroups, we can sequentially go through each subgroup
    #to generate mutants for each pdb in a given subgroup, and do this in a separate
    #process for each subgroup.
    
    pdb_list = os.listdir(pdb_folder)
    print(pdb_list)
    list_len = len(pdb_list)
    
    quot1 = int(list_len/np)
    pdb_array = [] #to contain several lists within one list. each sub list is assigned to a job during multiprocessing
    N = 0
    
    for i in range(0,np,1):
        pdb_array.append(pdb_list[N:N+quot1]) #add 1st quot1 items to the list
        N = N+quot1
    rem = (list_len % np)
    
    if rem != 0: #allocate remaining items to the pdb lists if necessary.
        
        for n in range(0,rem,1):
            pdb_array[n].append(pdb_list[N])
            N = N + 1
    
    start = time.time()
    
    print(pdb_array[:])
    
    for m in range(0,np,1): #spawn the processes
            
        proc_list = pdb_array[m]
        p = Process(target = mut_pack_pdb_seq, 
                    args = ( proc_list, #list of pdbs to access
                             pdb_folder, #Will still draw pdb files from the pdb folder
                             rosetta_scripts_exe, #Filepath of the rosetta scripts executable
                             relax_protocol, #XML you'd like to use for relax protocol
                             pack_protocol, #XML you'd like to use for the mutate and pack protocol
                             score_protocol,
                             output_dir, #parent dir for all output directories
                             ncaa_list,
                             residue_list_filepath, #list of chain/residue position to mutate
                             relax_commands,
                             pack_commands,
                             mut_res_commands,
                             ))
        p.start()
        if m == np-1:
            p.join() #hold off on calling the end until last process started terminates.

    end = time.time()
    print(end-start)    
    