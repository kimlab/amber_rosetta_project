#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 09:50:50 2017

@author: owenwhitley
"""

#Unit_tests

import os
import unittest
import sys

dirName = "amber_rosetta_project"

for root, dir, files, in os.walk("/home/kimlab2/owenwhitley"):
    if dirName in dir:
        proj_path = (os.path.join(root,dirName))
        break

test_path = os.path.join(proj_path,'rosetta_related/tests')
test_output_path = test_path + "/test_output"
ref_data_path = test_path + "/reference_data"
utils_path = os.path.join(test_path,"../rosetta_python_utils")
sys.path.append(utils_path)

import resFileUtilsV2 as RFU

class test_resfile_utils(unittest.TestCase):
    def setUp(self):
        RFU.make_resfile(pdbPath = os.path.join(ref_data_path,"4b7w_ref_for_resfile.pdb"), #filepath to pdb file you wish to generate resfile for
                     resfile_name = "4b7w_test_resfile_raw.resfile", #resfile name
                     destPath = test_output_path, #enter a string here for the destination folder (not a full directory path)
                     parent_directory = test_path, #parent directory of destination
                     new_directory = False, #boolean. Do we make a new directory?
                     overwrite = True, #boolean. do we overwrite an existing resfile?
                     Use_Input_SC = True, #boolean. If true, will write USE_INPUT_SC to first line
                                     #meaning that rosetta will allow experimentally determined rotamers
                                     #in the output structure it generates (if using packer)
                     allowables = ["ATOM"], #which lines do we allow? format as a list with string elements
                     logfile = True)
        self.ref_raw_res = open(os.path.join(ref_data_path,"4b7w_ref_raw_resfile.resfile"), "r")
        self.ref_raw_res_str = self.ref_raw_res.read()
        self.test_raw_res = open(os.path.join(test_output_path,"4b7w_test_resfile_raw.resfile"), "r")
        self.test_raw_res_str = self.test_raw_res.read()
        
                #make resfile array
        
        self.RFA = RFU.resfile_to_resFileArray(path = os.path.join(ref_data_path,
                                               "4b7w_ref_raw_resfile.resfile"),
                                               logfile = True,
                                               logfile_dir = test_output_path,
                                               logfile_name = "test_mod_resfile_log.txt",
                                               logfile_append = True)
        
        #perform edits on array
            
        self.single_edit = RFU.changeCommSingle(  resFileArray = self.RFA, 
                                                 chain = ["C"], 
                                                 position = 373, 
                                                 Commands = ['NATAA', 'EX 1', 'EX 2'],
                                                 logfile = True,
                                                 logfile_dir = test_output_path,
                                                 logfile_name = "test_mod_resfile_log.txt",
                                                 logfile_append = True)
        
        self.range_edit = RFU.changeCommRange(   resFileArray = self.RFA, 
                                                chain = ['D'], #Only single chain can be specified. Enter something like ['A'], to specify chain A, for example
                                                AAstart = 373, #First AA on chain to change commands for
                                                AAend = 375, #Last AA on chain to change commands for
                                                Commands = ['EMPTY', 'NC', 'EX 1', 'EX 2'],
                                                logfile = True,
                                                logfile_dir = test_output_path,
                                                logfile_name = "test_mod_resfile_log.txt",
                                                logfile_append = True)
        self.multi_chain_edit = RFU.changeCommChains(resFileArray = self.RFA,
                                         
                                                     chains = ['B','C','D'], 
                                                                 
                                                     Commands = ['NATAA', 'EX 1', 'EX 2'], 
                                                     logfile = True, 
                                                     logfile_dir = os.getcwd(), 
                                                     logfile_name = "test_mod_resfile_log.txt", 
                                                     logfile_append = True)
        self.all_chain_edit = RFU.changeCommChains(resFileArray = self.RFA,
                                         
                                                     chains = ['ALL'], 
                                                                 
                                                     Commands = ['NATAA', 'EX 1', 'EX 2'], 
                                                     logfile = True, 
                                                     logfile_dir = os.getcwd(), 
                                                     logfile_name = "test_mod_resfile_log.txt", 
                                                     logfile_append = True)
        
        #make the resfiles from respective resfile array variables
        
        RFU.array2ResFile(resFileArray = self.single_edit, 
                          directory = test_output_path, 
                          fName = '4b7w_test_C373_NATAA.resfile',
                          logfile = True,
                          logfile_dir = test_output_path,
                          logfile_name = "test_mod_resfile_log.txt",
                          logfile_append = True,
                          logIndicator = "single_edit_write")
        
        RFU.array2ResFile(resFileArray = self.range_edit, 
                          directory = test_output_path, 
                          fName = '4b7w_test_D373_D375_EMPTY.resfile',
                          logfile = True,
                          logfile_dir = test_output_path,
                          logfile_name = "test_mod_resfile_log.txt",
                          logfile_append = True,
                          logIndicator = "range_edit_write")
        
        RFU.array2ResFile(resFileArray = self.multi_chain_edit, 
                          directory = test_output_path, 
                          fName = '4b7w_test_chains_BCD.resfile',
                          logfile = True,
                          logfile_dir = test_output_path,
                          logfile_name = "test_mod_resfile_log.txt",
                          logfile_append = True,
                          logIndicator = "chain_edit_write")
        
        RFU.array2ResFile(resFileArray = self.all_chain_edit, 
                          directory = test_output_path, 
                          fName = '4b7w_test_all_chains.resfile',
                          logfile = True,
                          logfile_dir = test_output_path,
                          logfile_name = "test_mod_resfile_log.txt",
                          logfile_append = True,
                          logIndicator = "all_edit_write")
        
        #Make resfiles using make_resfile function
        
        RFU.edit_resfile(source_resfile = os.path.join(ref_data_path,"4b7w_ref_raw_resfile.resfile"),
                         output_dir = test_output_path, 
                         new_directory = False, 
                         fName = "4b7w_test_edit_single.resfile", 
                         overwrite_resfile = True,
                         edit_mode = "single", 
                         chains = ['C'], 
                         position = 373,
                         AAstart = None, 
                         AAend = None, 
                         Commands = ['NATAA', 'EX 1', 'EX 2'],
                         logfile = True, 
                         logfile_name = '4b7w_test_edit_resfile_single_log.txt',
                         logfile_append = False,
                         logIndicator = "edit_single")
        
        RFU.edit_resfile(source_resfile = os.path.join(ref_data_path,
                                                       "4b7w_ref_raw_resfile.resfile"), 
                         output_dir = test_output_path, 
                         new_directory = False, 
                         fName = "4b7w_test_edit_range.resfile", 
                         overwrite_resfile = True,
                         edit_mode = "range", 
                         chains = ['D'], 
                         position = None,
                         AAstart = 373, 
                         AAend = 375, 
                         Commands = ['EMPTY', 'NC', 'EX 1', 'EX 2'],
                         logfile = True, 
                         logfile_name = '4b7w_test_edit_resfile_range_log.txt',
                         logfile_append = False,
                         logIndicator = "edit_range")
        
        RFU.edit_resfile(source_resfile = os.path.join(ref_data_path,
                                                       "4b7w_ref_raw_resfile.resfile"), 
                         output_dir = test_output_path, 
                         new_directory = False, 
                         fName = "4b7w_test_edit_multi_chain.resfile", 
                         overwrite_resfile = True,
                         edit_mode = "chains", 
                         chains = ['B','C','D'], 
                         position = None,
                         AAstart = None, 
                         AAend = None, 
                         Commands = ['NATAA', 'EX 1', 'EX 2'],
                         logfile = True, 
                         logfile_name = '4b7w_test_edit_resfile_multichain_log.txt',
                         logfile_append = False,
                         logIndicator = "edit_chains")
        
        RFU.edit_resfile(source_resfile = os.path.join(ref_data_path,
                                                       "4b7w_ref_raw_resfile.resfile"), 
                         output_dir = test_output_path, 
                         new_directory = False, 
                         fName = "4b7w_test_edit_all_chains.resfile", 
                         overwrite_resfile = True,
                         edit_mode = "chains", 
                         chains = ['ALL'], 
                         position = None,
                         AAstart = None, 
                         AAend = None, 
                         Commands = ['NATAA', 'EX 1', 'EX 2'],
                         logfile = True, 
                         logfile_name = '4b7w_test_edit_resfile_allchains_log.txt',
                         logfile_append = False,
                         logIndicator = 'edit_all_chains')
        
        #Create stings for reference and output files, and test if contents equivalent
        
        #Do this for the output of the 'base functions', the output of the
        #edit_resfile function, and the reference.
        self.single_edit_test_str = (open(os.path.join(test_output_path,'4b7w_test_C373_NATAA.resfile'))).read() #produced by base functions
        self.single_edit_test_str2 = (open(os.path.join(test_output_path,"4b7w_test_edit_single.resfile"))).read() #produced by edit_resfile
        self.single_edit_ref_str = (open(os.path.join(ref_data_path,'4b7w_ref_C373_NATAA.resfile'))).read() #reference file
        
        self.range_edit_test_str = (open(os.path.join(test_output_path,'4b7w_test_D373_D375_EMPTY.resfile'))).read()
        self.range_edit_test_str2 = (open(os.path.join(test_output_path,"4b7w_test_edit_range.resfile"))).read()
        self.range_edit_ref_str = (open(os.path.join(ref_data_path,'4b7w_ref_D373_D375_EMPTY.resfile'))).read()
        
        self.multi_chain_test_str = (open(os.path.join(test_output_path,'4b7w_test_chains_BCD.resfile'))).read()
        self.multi_chain_test_str2 = (open(os.path.join(test_output_path,"4b7w_test_edit_multi_chain.resfile"))).read()
        self.multi_chain_ref_str = (open(os.path.join(ref_data_path,'4b7w_ref_chains_BCD.resfile'))).read()
        
        self.all_chain_test_str = (open(os.path.join(test_output_path,'4b7w_test_all_chains.resfile'))).read()
        self.all_chain_test_str2 = (open(os.path.join(test_output_path,"4b7w_test_edit_all_chains.resfile"))).read()
        self.all_chain_ref_str = (open(os.path.join(ref_data_path,'4b7w_ref_NATAA_EX1_EX2.resfile'))).read()
        
    def test_expected_outputs(self):
        
        self.assertEqual(self.ref_raw_res_str, self.test_raw_res_str)
        
        
        def count_line_matches(string1,string2):
            #idea: split strings by line, then the lines by whitespace
            #compare contents of each line, and count matches
            #if you don't have two strings with equal number of lines,
            #by default count = 0, and tests using this will fail
            lines_str1 = string1.split('\n') #list of line entries for string 1
            lines_str2 = string2.split('\n')
            
            if len(lines_str1) == len(lines_str2):
                count = 0
                
                for i in range(0,len(lines_str1),1):
                    strsplit_line1 = lines_str1[i].split()
                    strsplit_line2 = lines_str2[i].split()
                    
                    if strsplit_line1 == strsplit_line2:
                        count = count + 1
            else:
                count = 0
            
            return(count)
        
        def count_lines(string):
            return(len(string.split('\n')))
                
            
        self.assertEqual(count_line_matches(self.single_edit_test_str,
                                            self.single_edit_ref_str),
                        count_lines(self.single_edit_test_str))
        
        self.assertEqual(count_line_matches(self.single_edit_test_str2,
                                            self.single_edit_ref_str),
                        count_lines(self.single_edit_test_str2))
            
            
        self.assertEqual(count_line_matches(self.range_edit_test_str,
                                            self.range_edit_ref_str),
                        count_lines(self.range_edit_test_str))
            
        self.assertEqual(count_line_matches(self.range_edit_test_str2,
                                            self.range_edit_ref_str),
                        count_lines(self.range_edit_test_str2)) 
        
            
        self.assertEqual(count_line_matches(self.multi_chain_test_str,
                                            self.multi_chain_ref_str),
                        count_lines(self.multi_chain_test_str))
            
        self.assertEqual(count_line_matches(self.multi_chain_test_str2,
                                            self.multi_chain_ref_str),
                        count_lines(self.multi_chain_test_str2))
 
           
        self.assertEqual(count_line_matches(self.all_chain_test_str,
                                            self.all_chain_ref_str),
                        count_lines(self.all_chain_test_str))

        self.assertEqual(count_line_matches(self.all_chain_test_str2,
                                            self.all_chain_ref_str),
                        count_lines(self.all_chain_test_str2))
    
if __name__ == '__main__':
    unittest.main()