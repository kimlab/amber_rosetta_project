#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 17:28:54 2017

@author: owenwhitley
"""
import os
import sys

#An example of 

amber_rosetta_dir = '/home/kimadmin/amber_rosetta_project'
rosetta_python_utils = os.path.join(amber_rosetta_dir,'rosetta_related/rosetta_python_utils')
sys.path.append(rosetta_python_utils)
import rosetta_interfaces as RI
import analysis_utils as AU

pdb_folder = 'prepped_for_rosetta'
output_dir = os.getcwd()
input_dir = os.path.join(output_dir,'input_files')
#rosetta_scripts_exe = '/home/kimlab2/owenwhitley/rosetta_3.8_2017/main/source/bin/rosetta_scripts.static.linuxgccrelease'
residue_list = 'residue_list.txt'
my_ncaa_list = 'amber_valid_ncaas_short.txt'

RI.multiprocess_rosetta_mut( pdb_folder,
                             input_dir,
                             output_dir,
                             rosetta_scripts_exe,
                             residue_list,
                             amber_rosetta_proj_dir = amber_rosetta_dir,
                             num_processes = 3,
                             relax_protocol = None,
                             pack_protocol = None,
                             score_protocol = None,
                             relax_commands = None,
                             pack_commands = None,
                             mut_res_commands = None,
                             ncaa_list = my_ncaa_list)


#generate score list, ranking by group then score. can also rank by rosetta score
#see analysis_utils under rosetta_related/rosetta_python_utils for details
score_list = AU.make_scoretable_ensemble(rosetta_ensemble_dir = output_dir, 
                                  ensemble_name = '5q0k', 
                                  overwrite = True,
                                     cutoff = False,
                                     rlx_diff_cutoff = 0.0,
                                     cln_diff_cutoff = 0.0,
                                     rank_by = 'group')


#Below function needs testing to see if output values correct
AU.rank_muts(rosetta_scoretable_list = score_list,
              top_cluster_file = 'cluster_output_md8_interface_0.75_best_matches.txt', 
              include_cluster_weights = True, 
              output_dir = os.getcwd(),
              filename_prefix = 'mut_rank_non_wt_5q0k_clusters_',
              overwrite = True)
