#!/bin/bash
#PBS -l nodes=20:ppn=8:,walltime=1:00:00
cd $PBS_O_WORKDIR
module purge
module load intel/15.0.6
module load intelmpi/5.0.3.048
module load gcc/4.8.1
module load cuda/6.0
module load amber
mpirun -np 80 pmemd.MPI -ng 10 -groupfile 5q0k_muts4MD_mut_3_prod_step1_groupfile.txt 
