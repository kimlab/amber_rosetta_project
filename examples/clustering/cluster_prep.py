#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 14:05:31 2017

@author: owenwhitley
"""
import os
import sys

amber_rosetta_dir = '/home/kimlab2/owenwhitley/amber_rosetta_project'
amber_utils = os.path.join(amber_rosetta_dir,'amber_related/amber_python_utils')

sys.path.append(amber_utils)

import amber_clustering as AC



input_dir = os.getcwd()
top = '5q0k_prepped.prmtop'
output_dir = os.path.join(input_dir,'analysis_clustering')
prod_frames = 4500 #
num_chains = 2
ref = '5q0k_rlx_sys_imin.rst'
ligand_range = [231,244]
target_range = [1,230]

#NOTE: MD simulation was run with 100 ps equilibrating solvent, 100 ps equilibrating system
# 900 ps production. Each step of md was 0.002 ps and structure written to mdcrd file every 100 steps
# So, we take 4500 frames for the length of the production step. The function takes the last 
# 4500 frames of simulation, which corresponds to production

#Also note that as of now, you CANNOT enter an integer for ref (i.e. references structure)



AC.make_pdbs(input_dir, #string, path to input directory
              top, #name, not path, of topology file, located in input_dir
              output_dir, #string, path to output directory
              prod_frames, #the number of frames corresponding to the production phase of MD
                          #Will make pdb files for the last (prod_frames) frames of entire trajectory
              num_chains = num_chains, #number of chains in structure, integer
              ref = ref, #reference structure for calculating RMSD. expected to be rst file for system minimization (structure just prior to MD)
              ligand_range = ligand_range, #range of residues #s in structure corresponding to ligand, enter as a list of two integers,
                          #with the lower number corresponding to the first residue and the higher number corresponding to the last
              target_range = target_range, #same as ligand_range, but corresponding to the range of residues in structure corresponding to
                          #the last 
              mask = '!:WAT,CL-,Na+', #mask for pytraj to select residues/atoms for the trajectory that will be written to output
              mass = True, #mass weight RMSD?
              rmsd_lim = 5.0, #If rmsd at any frame every goes above this value, do not write pdb to output
              interface_thresh = 8.0 #threshold distance between residues to define interface, unit = Angstroms
              )

#After generating all outputs in output_dir, you would typically
#go into that directory, and would expect to find a folder full of full pdb structures
#corresponding to particular frames of MD simulations, an folder ontaining rmsd information and plots for each
#simulation, as well as a folder containing pdbs having only the interface of the ligand protein interaction,
#corresponding to each frame of MD simulation.

#To perform clustering, you'd make a bash script such as this 
#!/bin/bash
#cluster.pl -kclust -iterate  -radius 0.75 -mode rmsd -centroid -pdb ./interface_pdbs/*.pdb > cluster_output_md8_interface_0.75.txt

#This uses the cluster.pl script from mmtsb, using fixed radius k means clustering, with a cluster radius of 0.75 A,
#clustering by pairwise RMSD between structures. This perofrms clustering using all input pdbs in the interface pdbs
#directory, and writes output to the file cluster_output_md8_interface_0.75.txt.

#Once the clustering output file is present, the next step would be to 
#use the clust_parser function to parse the cluster output file, generate 
# a file containing the clusters with elements ordered by RMSD to centroid,
#and a file containing the names of PDB files who are closest to their respective clusters' centroids.

# Afterwards the retrieve pdbs function will, given your list of 
# closest to centroid interface pdbs, find the corresponding full length pdb,
#and put to the stated output directory. The retrieved pdbs will serve as our
#'ensemble' from clustering that will be put into the rosetta mutation
#component of the pipeline, after a preprocessing step discussed in the USER MANUAL.
#See the USER MANUAL or amber_conversion.py under rosetta_related/rosetta_python_scripts for
#more details

 #Note that an interface pdb file us derived from a full pdb structure, 
 #being sliced to have only atom coordinates
#for interface residues. 
#import clust_parser as CP

#CP.clust_parser(input_file, #should be an output file from cluster.pl script, which ran using kclust option
#                 overwrite = False)

#CP.retrieve_pdbs(match_file, #the file corresponding to shortlist_fileobj in above function,
#                              containing cluster IDs, counts, fraction of population,
#                              pdb file corresponding to structure closest to cluster centroid
#                  pdb_frame_dir, #directory containing all the single frame pdbs.
#                                  #you can use this to select the directory containing the pdbs
#                                  #you did clustering on, or, if you did clustering on pdbs containing
#                                  #structure interfaces, you can select a directory with the corresponding
#                                  #full length structures.
#                  output_dir, wdir = os.getcwd(), thresh = 0.01)