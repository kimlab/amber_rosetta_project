#!/bin/bash
#PBS -l nodes=20:ppn=8:,walltime=1:00:00
cd $PBS_O_WORKDIR
module purge
module load intel/15.0.6
module load intelmpi/5.0.3.048
module load gcc/4.8.1
module load cuda/6.0
module load amber
mpirun -np 12 pmemd.cuda -O -i 2zix_ext_testrun_1_heat_solv_md.in -o 2zix_ext_testrun_1_heat_solv_md10.mdout -c 2zix_ext_testrun_1_rlx_sys_imin.rst -p 2zix_ext_testrun_1.prmtop -x 2zix_ext_testrun_1_heat_solv_md10.mdcrd -ref 2zix_ext_testrun_1_rlx_sys_imin.rst -r 2zix_ext_testrun_1_heat_solv_md10.rst
mpirun -np 12 pmemd.cuda -O -i 2zix_ext_testrun_1_heat_sys_md.in -o 2zix_ext_testrun_1_heat_sys_md10.mdout -c 2zix_ext_testrun_1_heat_solv_md10.rst -p 2zix_ext_testrun_1.prmtop -x 2zix_ext_testrun_1_heat_sys_md10.mdcrd -ref 2zix_ext_testrun_1_rlx_sys_imin.rst -r 2zix_ext_testrun_1_heat_sys_md10.rst
mpirun -np 12 pmemd.cuda -O -i 2zix_ext_testrun_1_prod_md.in -o 2zix_ext_testrun_1_prod_step1_md10.mdout -c 2zix_ext_testrun_1_heat_sys_md10.rst -p 2zix_ext_testrun_1.prmtop -x 2zix_ext_testrun_1_prod_step1_md10.mdcrd -r 2zix_ext_testrun_1_prod_step1_md10.rst
