
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 09:24:34 2017

@author: owenwhitley
"""
import sys
import os


ambrose_dir= '/home/kimlab2/owenwhitley/amber_rosetta_project' #directory of amber_rosetta project for pipeline scripts

ambdir = os.path.join(ambrose_dir,'amber_related')
amb_utils = os.path.join(ambdir,'amber_python_utils')
sys.path.append(amb_utils)

wdir = os.getcwd()
crdfile = '2zix_ext.crd'
top = '2zix_ext.top'

import prep_from_rosetta as PR


PR.prep_md_inputs( mode = 'denovo', #are we taking pdbs from rosetta or doing this de novo?
                   ambdir = ambdir,
                   input_mode = 'cuda',
                   num_simulations = 10,
                   num_prod_steps = 3,
                   proc_per_sim = 12,
                   script_template = 'script_template.sh',
                   num_residues = 115, #required to be of type int if using any of default protocols
                   rlx_mode = 'default',
                   eq_mode = 'default',
                   prod_mode = '10ns',
                   wdir = wdir, #required if mode = denovo
                   crdfile = crdfile, #required if mode = denovo, crdfile from H++ website
                   prmtop = top, #required if mode = denovo, topology file from H++ website
                   expt_name = '2zix_ext_testrun_1', #required if mode = denovo, variable will
                   #be specified automatically if mode = rosetta
                   output_dir = None, #required if mode = rosetta, 
                   #must be a string giving filepath to soon-to-exist folder 
                   #where subdirectories will contain all input files to run an md
                   #for a given rosetta generated pdb (one subfolder made for each)
                   #pdb in the pdb_folder from which we will be generating md inputs
                   pdb_folder = None, #required if mode = rosetta, 
                   #must be a string giving filepath to folder 
                   #containing pdbs genearted by rosetta
                   solvate = True, #if true, use explicit solvent with periodic boundaries
                   add_ions = 'Cl-', #not strictly required, but needed if protein has a net charge
                   box_length = 11, #requred if solvate = True
                   forcefield = 'leaprc.ff14SB')